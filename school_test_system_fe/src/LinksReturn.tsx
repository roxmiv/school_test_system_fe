import {FunctionComponent, useContext, useEffect, useState} from "react";
import {ApiServices} from "./contexts/ApiStore";
import { Link } from 'react-router-dom';
import "./App.css";
import { MainUser } from "./models/users_auth";

interface LinksReturnProps {
}

export const LinksReturn: FunctionComponent<LinksReturnProps> = () => {
    const {tasksService} = useContext(ApiServices);
    const [user, setUser] = useState<MainUser|null>(null);

    useEffect(() => {
        tasksService.getUser().then((data: MainUser) => {
            setUser(data);
        },)
    }, [tasksService]);

    return (
        <div>
            <div>
                {user?.is_teacher ?
                    <li><Link className='link_center' to="/create_task/1">Создать задание</Link></li>
                : null }
                {user?.is_school_admin ?
                <li><Link className='link_center' to="/users">Пользователи</Link></li>
                : null }
            </div>
            <div>
                {user ?
                <div>
                    <li><Link className='link_right' to="/personal_area">{user?.last_name} {user?.last_name}</Link></li>
                </div>
                : 
                <div>
                    <li><Link className='link_right' to="/registration">Регистрация</Link></li>
                    <li><Link className='link_right' to="/login">Войти</Link></li>
                </div>
                }
            </div>
        </div>
    );
};