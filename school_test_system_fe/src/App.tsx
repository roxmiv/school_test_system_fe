import {useContext} from "react";
import {ApiServices} from "./contexts/ApiStore";
import {Route, Switch, Router, Redirect} from "react-router-dom";
import {createBrowserHistory} from 'history';
import {SnackbarProvider} from 'notistack';
import {ApiStore} from "./contexts/ApiStore";
import {routes} from "./common/routes";
import { Link } from "react-router-dom";
import { LinksReturn } from "./LinksReturn";
import "./App.css";

export function App() {
  const history = createBrowserHistory();

  return (
      <div>
        <Router history={history}>
          <SnackbarProvider maxSnack={5} autoHideDuration={3000}>
            <ApiStore>
                <ul className='links'>
                  <li><Link className='link_center' to="/home">Главная</Link></li>
                  <li><Link className='link_center' to="/tasks/grades">Задания</Link></li>
                  <li><Link className='link_center' to="/tests">Тесты</Link></li>
                  <LinksReturn />
                </ul>
                <Switch>
                  {routes.map((route, index) => <Route key={index} {...route} />)}
                  <Redirect to="/home"/>
                </Switch>
            </ApiStore>
          </SnackbarProvider>
        </Router>
      </div>
  );
}
