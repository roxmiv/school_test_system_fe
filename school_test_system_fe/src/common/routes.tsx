import { MainPage } from "../pages/Main";
import { GradesListPage } from "../pages/GradesList";
import { SubjectsListPage } from "../pages/SubjectsList";
import { SubjectsTests } from "../pages/SubjectsTests";
import { TopicsListPage } from "../pages/TopicsList";
import { TasksListPage } from "../pages/TasksList";
import { TaskPage } from "../pages/Task";
import { EditTask } from "../pages/EditTask";
import { UsersPage } from "../pages/UsersPage";
import { UserInfo } from "../pages/UserInfo";
import { PersonalArea } from "../pages/PersonalArea";
import { TestsTests } from "../pages/TestsTests";
import { TestsCompletedTests, PrintCompletedTest } from "../pages/TestsCompletedTests";
import { TestStartTestPage } from "../pages/TestStartTestPage";
import { DoTest } from "../pages/DoTest";
import EndText from "../pages/EndText";
import CreateRoles from "../pages/CreateRoles";
import CreateTask from "../pages/CreateTask";
import CreateUser from "../pages/CreateUser";
import LoginPage from "../pages/LoginPage";
import CreateTest from "../pages/CreateTest";

export enum RoutesMap {
    HOME = "/home",
    REGISTRTION = "/registration",
    LOGIN = "/login",
    USERS = "/users",
    EDIT_TASK = "/edit_task/:param",
    PERSONAL = '/personal_area',
    USER = "/users/user/:param",
    CREATE_ROLE = "/users/create_role/",
    GRADES_TASK = "/tasks/grades",
    SUBJECTS_TASK = "/tasks/grades/:param",
    TOPICS_TASK = "/tasks/grades/subjects/:param",
    TASKS_TASK = "/tasks/grades/subjects/topics/:param",
    TASK_TASK = "/tasks/grades/subjects/topics/tasks/:param",
    CREATE_TASK = "/create_task/:param",
    TESTS_TEST = "/tests",
    DO_TEST = "/tests/process/:param",
    SUBJECTS_TEST = "/tests/grades/:param",
    TOPICS_TEST = "/tests/grades/subjects/:param",
    TESTS_COMPLETED_TESTS = "/tests/completed_tests",
    TESTS_START_TEST_PAGE = "/tests/:param",
    TEST_END = "/tests/test/end",
    PRINT_COMPLETED_TEST = "/tests/completed_tests/:param",
    CREATE_TEST = "/tests/teacher/create_test",
}

export const routes = [
    { path: RoutesMap.HOME, component: MainPage, exact: true },
    { path: RoutesMap.SUBJECTS_TASK, component: SubjectsListPage, exact: true },
    { path: RoutesMap.GRADES_TASK, component: GradesListPage, exact: true },
    { path: RoutesMap.TOPICS_TASK, component: TopicsListPage, exact: true },
    { path: RoutesMap.TASKS_TASK, component: TasksListPage, exact: true },
    { path: RoutesMap.TASK_TASK, component: TaskPage, exact: true },
    { path: RoutesMap.EDIT_TASK, component: EditTask, exact: true },
    { path: RoutesMap.TESTS_TEST, component: TestsTests, exact: true },
    { path: RoutesMap.DO_TEST, component: DoTest, exact: true },
    { path: RoutesMap.SUBJECTS_TEST, component: SubjectsTests, exact: true },
    { path: RoutesMap.USERS, component: UsersPage, exact: true },
    { path: RoutesMap.USER, component: UserInfo, exact: true },
    { path: RoutesMap.PERSONAL, component: PersonalArea, exact: true },
    {path: RoutesMap.TESTS_COMPLETED_TESTS, component: TestsCompletedTests, exact: true},
    {path: RoutesMap.TESTS_START_TEST_PAGE, component: TestStartTestPage, exact: true},
    {path: RoutesMap.PRINT_COMPLETED_TEST, component: PrintCompletedTest, exact: true},
    {path: RoutesMap.TEST_END, component: EndText, exact: true},
    { path: RoutesMap.REGISTRTION, component: CreateUser, exact: true },
    { path: RoutesMap.LOGIN, component: LoginPage, exact: true },
    { path: RoutesMap.CREATE_TASK, component: CreateTask, exact: true },
    { path: RoutesMap.CREATE_ROLE, component: CreateRoles, exact: true },
    {path: RoutesMap.CREATE_TEST, component: CreateTest, exact: true},
];