import React, {FunctionComponent, useCallback} from 'react';
import {useNotification} from "../hooks/Notifications";
import tasksService from "../services/tasksService";
import {api} from "../services/api";

export const ApiServices = React.createContext<{
    tasksService: any,
}>(null as any);

export const ApiVersion = React.createContext(null);

interface ApiStoreProps {
    children: any;
}

export const ApiStore: FunctionComponent<ApiStoreProps> = ({children}) => {
    const notification = useNotification();
    const resHandler = useCallback((res: { data: { data: any; }; }) => {
        if (res.data && res.data.data) {
            return res.data.data;
        } else {
            return null;
        }
    }, []);
    const errorHandler = useCallback((error: { response: { data: { error: { text: string; }; redirect: string; }; }; message: string; error: { error: { message: string; }; }; }) => {
        if (error.response && error.response.data && error.response.data.error) {
            notification(error.response.data.error.text, 'error');
        }else if (error.response && error.response.data && error.response.data.redirect) {
            notification(error.response.data.redirect, 'info');
        } else if (error instanceof Error) {
            notification(error.message, 'error');
        } else {
            notification(error.error.error.message, 'error');
        }
    }, [notification]);

    api.interceptors.response.use(resHandler, errorHandler);

    const services = {
        tasksService: tasksService(api),
    };

    return (
        <ApiServices.Provider value={services}>
            {children}
        </ApiServices.Provider>
    );
};
