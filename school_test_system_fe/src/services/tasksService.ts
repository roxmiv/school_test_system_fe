import {Grade, Subject, Topic, Task, GradeDifficulty} from "../models/Tasks";
import {MainUser} from "../models/users_auth";
import {AxiosInstance} from "axios";
import {StudentTest, Test, Variant} from "../models/Tests";

const apiService = (api: AxiosInstance) => ({
    getDifficultiesList(id: number): Promise<GradeDifficulty[]> {
        return api.get('/tasks/gradedifficultys/list/' + id + '/');
    },
    getSubjectName(id: number): Promise<Subject> {
        return api.get('/tasks/get_subject_name/' + id + '/');
    },
    getGradeName(id: number): Promise<Grade> {
        return api.get('/tasks/get_grade_name/' + id + '/');
    },
    getTopic(id: number): Promise<Topic> {
        return api.get('/tasks/get_topic_by_id/' + id + '/');
    },
    getTask(id: number): Promise<Task> {
        return api.get('/tasks/get_task_by_id/' + id + '/');
    },
    getGradesList(): Promise<Grade[]> {
        return api.get('/tasks/grades/list/');
    },
    getSubjectsList(): Promise<Subject[]> {
        return api.get('/tasks/subjects/list/');
    },
    getTopicsList(id: number): Promise<Topic[]> {
        return api.get('/tasks/subjects/list/' + id + '/');
    },
    getAllTopics(): Promise<Topic[]> {
        return api.get('/tasks/all_topics/list/');
    },
    getSubjectsListByGrade(id: number): Promise<Subject[]> {
        return api.get('/tasks/grade/list/' + id + '/');
    },
    getTasksList(id: number): Promise<Task[]> {
        return api.get('/tasks/subjects/list/topics/' + id + '/');
    },
    getUser(): Promise<MainUser> {
        return api.get('/users_auth/get_user/');
    },
    getUsersList(): Promise<MainUser[]> {
        return api.get('/users_auth/get_users_list/');
    },
    getUserById(id: number): Promise<MainUser> {
        return api.get('/users_auth/get_user_by_id/' + id + '/');
    },
    login(elem: {username: string, password: string}) : Promise<number> {
        return api.post('/users_auth/login/', {
                                data: elem,                            
                                headers: {
                                    'Access-Control-Allow-Origin': 'http://localhost:3000/',
                                },
                                withCredentials: true,
                            }
                        );
    },
    logout(data: {}) : Promise<number> {
        return api.post('/users_auth/logout/', {data: data});
    },
    changePassword(elem: {username: string, new_password: string}): Promise<number> {
        return api.post('/users_auth/change_password_for_user/', {data: elem});
    },
    createUser(elem: {username: string, password: string, first_name: string, last_name: string}) : Promise<number> {
        return api.post('/users_auth/create_main_user/', {data: elem});
    },
    createSubject(subject: Subject, grade: Grade): Promise<number> {
        return api.post('/tasks/create_subject/', {data: subject});
    },
    createGrade(grade: Grade): Promise<number> {
        return api.post('/tasks/create_grade/', {data: grade});
    },
    createTopic(grade: Grade): Promise<number> {
        return api.post('/tasks/create_topic/', {data: grade});
    },
    createTask(task: Task): Promise<number> {
        return api.post('/tasks/create_task/', {data: task});
    },
    editTask(task: Task): Promise<number> {
        return api.post('/tasks/edit_task/', {data: task});
    },
    editTopic(topic: Topic): Promise<number> {
        return api.post('/tasks/edit_topic/', {data: topic});
    },
    deleteTask(task: Task): Promise<number> {
        return api.post('/tasks/delete_task/', {data: task});
    },
    createTeacher(user: {main_user: MainUser}): Promise<number> {
        return api.post('/users_auth/create_teacher/', {data: user});
    },
    createStudent(user: {main_user: MainUser}): Promise<number> {
        return api.post('/users_auth/create_student/', {data: user});
    },
    getStudentTestsList() : Promise<StudentTest[]> {
        return api.get('/tasks/student_tests/list/')
    },
    getVariantsList() : Promise<Variant[]> {
        return api.get('/tasks/variants/list/')
    },
    generateVariant(test: any) : Promise<Variant> {
        return api.post('/tests/test_generation/', {data: test})
    },
    getStudentTestsById(id: number): Promise<StudentTest> {
        return api.get('/tasks/get_student_test_by_id/' + id +'/')
    },
    getVariant(id: number): Promise<Variant> {
        return api.get('/tasks/get_variant_by_id/' + id + '/')
    },
    saveAnswer(data: any): Promise<number> {
        return api.post('/tests/save_variant_task_answer/', {data: data})
    },
    checkAnswer(data: any): Promise<number> {
        return api.post('/tests/check_variant_answers/', {data: data})
    },
    createTest(test: Test) : Promise<number> {
        return api.post('tests/create_test/', {data: test});
    },
    GetDifficultyByGrade(grade_id: number, diff: number): Promise<GradeDifficulty> {
        return api.get('/tasks/get_difficulty_by_grade/' + grade_id + '/' + diff + '/');
    }
})

export default apiService;