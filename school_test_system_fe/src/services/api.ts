import axios, {AxiosInstance} from "axios";

export const headers = {
};

export const api: AxiosInstance = axios.create({
    baseURL: "http://localhost:8000/api/",
    withCredentials: true,
});

