import { NumberLiteralType } from 'typescript'
import { AnswersType, GradeDifficulty, MaskTask, Question } from './Tasks'
import { Subject, QuestionAnswer } from './Tasks'
import { Topic } from './Tasks'
import { MainUser, Student, Teacher } from './users_auth'

export {}

export class Test {
    id!: number;
    title!: string;
    grade_difficulty!: GradeDifficulty;
    subject!: Subject;
    author!: MainUser;
    test_tasks!: TestTask[];
    training_possible!: boolean;
    duration!: Duration;
    duration_in_minutes!: number;
    student_tests!: StudentTest[];
}

export class Duration {
    days!: number;
    hours!: number;
    minutes!: number;
}

export class TestTask {
    id!: number;
    topic!: Topic;
    position!: number;
    test!: Test;
    grade_difficulty!: GradeDifficulty;
}

export class StudentTest {
    id!: number;
    student!: Student;
    teacher!: Teacher;
    starttime!: Date;
    deadline!: Date;
    training_starttime!: Date;
    training_deadline!: Date;
    num_attemps!: number;
    test!: Test;
    score!: number;
}

export class Variant {
    id!: number;
    student!: Student;
    student_test!: StudentTest;
    score!: number;
    max_score!: number;
    variant_tasks!: VariantTask[];
    deadline!: number;
    is_control!: boolean;
}

export class VariantTask {
    id!: number;
    mask_task!: MaskTask;
    position!: number;
    variant!: Variant;
    variant_task_answers!: VariantTaskAnswer[];
}

export class VariantTaskAnswer {
    id!: number;
    variant_task!: VariantTask;
    question!: Question;
    answer!: QuestionAnswer;
    is_correct!: boolean;
    text!: string;

}