import { MainUser, Student } from './users_auth'
import { Teacher } from './users_auth'
import {Test, TestTask, VariantTask, VariantTaskAnswer} from './Tests'

export class Grade {
    id!: number;
    name!: string;
}

export class Subject {
    id!: number;
    name!: string;
    grade!: Grade;
    teachers!: Teacher[];
    students!: Student[];
    topics!: Topic[];
}

export class Topic {
    id!: number;
    name!: string;
    subjects!: Subject[];
}

export enum Difficult {
    EASY = 0,
    MEDIUM = 1,
    HARD = 2,
}

export class GradeDifficulty {
    id!: number;
    difficult!: Difficult;
    grade!: Grade;
}

export class Task{
    id!: number;
    text!: string;
    grade_difficulty!: GradeDifficulty[];
    topic!: Topic;
    author!: MainUser;
    is_training!: boolean;
    is_control!: boolean;
    questions!: Question[];
    date_time!: Date;
}

export class MaskTask {
    id!: number;
    text!: string;
    grade_difficulty!: GradeDifficulty[];
    topic!: Topic;
    author!: MainUser;
    is_training!: boolean;
    is_control!: boolean;
    questions!: Question[];
    date_time!: Date;
    task!: Task;
    is_final!: boolean;
}

export enum AnswersType {
    ACCURATE = 0,
    SELECT = 1,
    MULTISELECT = 2,
};

export class Question {
    id!: number;
    text!: string;
    ans_type!: AnswersType;
    answers!: QuestionAnswer[];
    position!: number;
}

export class QuestionAnswer {
    id!: number;
    text!: string;
    is_correct!: boolean;
}

// export class Image {
// task!: Task;
// question_answer!: QuestionAnswer;
// questions!: Question;
// image!: ? ;
//}

// export class File {
// task!: Task;
// task_answer!: QuestionAnswer;
// questions!: Question;
// file!: ? ;
//}