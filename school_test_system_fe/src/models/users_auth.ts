
import { Subject } from "./Tasks";
import { Test, StudentTest, Variant } from "./Tests";

export class MainUser {
    id!: number;
    username!: string;
    first_name!: string;
    last_name!: string;
    tests!: Test[];
    is_student!: boolean;
    is_teacher!: boolean;
    is_task_author!: boolean;
    is_school_admin!: boolean;
}

export class Group {
    id!: number;
    level!: number;
    name!: string;
}

export class Teacher {
    id!: number;
    main_user!: MainUser;
    subjects!: Subject[];
}

export class Student {
    id!: number;
    main_user!: MainUser;
    student_tests!: StudentTest[];
    variants!: Variant[];
}

export class SchoolAdmin {
    id!: number;
    main_user!: MainUser;
}

export class TasksAuthor {
    id!: number;
    main_user!: MainUser;
}