import {FunctionComponent, useContext, useEffect, useState} from "react";
import {ApiServices} from "../contexts/ApiStore";
import { Task } from "../models/Tasks";
import { MainUser } from "../models/users_auth";
import { useParams } from 'react-router-dom';
import Edition from "./Edition";

interface EditTaskProps {
}

interface ParamTypes {
    param: string;
}

export const EditTask: FunctionComponent<EditTaskProps> = () => {
    const {tasksService} = useContext(ApiServices);
    const {param} = useParams<ParamTypes>();
    const [task, setTask] = useState<Task|null>(null);
    const [user, setUser] = useState<MainUser|null>(null);

    useEffect(() => {
        tasksService.getTask(parseInt(param)).then((data: Task) => {
            setTask(data);
        },)
    }, [tasksService]);

    useEffect(() => {
        tasksService.getUser().then((data: MainUser) => {
            setUser(data);
        },)
    }, [tasksService]);

    const deleteTask = () => {
        tasksService.deleteTask(task);
        window.location.replace("/home");
    }

    return (
        <div>
            {user ?
                <div>
                    <div>
                        {task ? 
                            <Edition task={task}/>
                            : null
                        }
                    </div>
                    <button className='button4' onClick={deleteTask}>Удалить задание</button>
                </div>
            : null}
        </div>
    );
};