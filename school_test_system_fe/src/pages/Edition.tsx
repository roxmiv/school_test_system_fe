import React, { Component } from 'react';
import { ApiServices } from '../contexts/ApiStore';
import { AnswersType, Question, QuestionAnswer, Task } from '../models/Tasks';
import CreateQuestion from './CreateQuestion';
import CreateAnswer from './CreateAnswer';
import EditAnsw from './EditAnsw';
import EditText from './EditText';

class Edition extends React.Component<any, any> {
    static contextType = ApiServices;
    task: Task;

    constructor(props: any) {
        super(props);
        this.task = props.task;
        this.state = {
            value: false,
            pushValue: false,
            pushAnswValue: false,
            nameValue: false,
            name: props.task.text,
        }
    }
    
    editName = (propName: any) => {
        if (this.task) {
            let tasksService = this.context;
            this.task.text = propName;
            tasksService.tasksService.editTask(this.task);
            this.forceUpdate();
        }
    }

    deleteQuestion = async (a: any) => {
        if (this.task) {
            let tasksService = this.context;
            this.task.questions = this.task.questions.filter(elem => elem.id != a);
            console.log(this.task);
            tasksService.tasksService.editTask(this.task);
            this.forceUpdate();
        }
    }

    pushQuestion = async (props: Question) => {
        if (this.task) {
            let tasksService = this.context;
            this.task.questions.push(props);
            tasksService.tasksService.editTask(this.task);
            this.forceUpdate();
        }
    }

    pushAnswer = async (ans: QuestionAnswer, quest: Question) => {
        if (this.task) {
            let tasksService = this.context;
            let ind = this.task.questions.findIndex(el => el == quest);
            this.task.questions[ind].answers.push(ans);
            tasksService.tasksService.editTask(this.task);
            this.forceUpdate();
        }
    }

    Rendering = (props: {value: boolean}) => {
        const value = props.value;
        if (value) {
            console.log(this.task.questions[this.task.questions.length-1].position);
            console.log(this.task);
            return <CreateQuestion func={this.pushQuestion}
                                   pos={this.task.questions.length}/>
        } else {
            return null;
        }
    }

    Rendering2 = (props: {value: boolean, question: Question}) => {
        const value = props.value;
        if (value) {
            return <CreateAnswer func={this.pushAnswer} quest={props.question}/>
        } else {
            return null;
        }
    }

    update = () => {
        this.forceUpdate();
    }

    deleteAnsw = (quest: Question, answ: any) => {
        let tasksService = this.context;
        let ind = this.task.questions.findIndex((el: Question) => el == quest);
        console.log(this.task.questions[ind]);
        this.task.questions[ind].answers = this.task.questions[ind].answers.filter((el) => el != answ);
        tasksService.tasksService.editTask(this.task);
        this.forceUpdate();
    }

    handleChange = (quest: Question, e: any) => {
        let tasksService = this.context;
        let ind = this.task.questions.findIndex((el: Question) => el == quest);
        this.task.questions[ind].ans_type = e.target.value;
        tasksService.tasksService.editTask(this.task);
        this.forceUpdate();
    }

    render() {
        return(
            <div>
                <br></br>
                <br></br>
                <br></br>
                <header>
                    <div className='boldtext'>
                        {this.task?.text}
                    </div>
                    <button className='button4' onClick={() => this.setState({nameValue: !this.state.nameValue})}>Редактировать</button>
                    {this.state.nameValue ? 
                    <>
                        <input
                            type="text"
                            value={this.state.name}
                            onChange={e => this.setState({name: e.target.value})}
                        />
                        <button className='button4_2' onClick={this.editName.bind(null, this.state.name)}>Сохранить</button>
                    </>
                    : null }
                </header>
                <div className='list11'>
                    <>{(this.task?.is_control && this.task?.is_training) ? 'Задание контрольное, тренировочное' : null}</>
                    <>{(this.task?.is_control && !this.task?.is_training) ? 'Задание контрольное': null }</>
                    <>{(!this.task?.is_control && this.task?.is_training) ? 'Задание тренировочное': null }</>
                </div>
                <button className='button4' onClick={() => this.setState({value: !this.state.value})}>Редактировать</button>
                {this.state.value ? 
                <div>
                <button 
                    className={this.task.is_control ? 'button3_2' : 'button3'}
                    onClick={() => {    this.task.is_control = !this.task.is_control;
                                        let tasksService = this.context;
                                        tasksService.tasksService.editTask(this.task);
                                        this.forceUpdate(); 
                                    }}
                >
                        Задание контрольное
                </button>
                <button
                    className={this.task.is_training ? 'button3_2' : 'button3'}
                    onClick={() => {    this.task.is_training = !this.task.is_training;
                                        let tasksService = this.context;
                                        tasksService.tasksService.editTask(this.task);
                                        this.forceUpdate(); 
                                    }}
                >
                        Задание тренировочное
                </button>
                </div>
                : null}
                <ul>
                    {this.task?.questions && this.task?.questions.map((question, index) =>
                    <li className='list11' key={index}>
                        {question.text}
                        <select className='select' onChange={e => {
                            let tasksService = this.context;
                            let ind = this.task.questions.findIndex((el: Question) => el == question);
                            this.task.questions[ind].ans_type = parseInt(e.target.value);
                            if (this.task.questions[ind].ans_type == 0) {
                                this.task.questions[ind].answers = new Array<QuestionAnswer>();
                            }
                            tasksService.tasksService.editTask(this.task);
                            this.forceUpdate();
                        }}>
                            <option className='select_head'>Выберите тип вопроса</option>
                            {question.ans_type == 0 ? 
                                <option selected={true} className='select_item' value={AnswersType.ACCURATE}>Развернутый ответ</option>
                            :   <option className='select_item' value={AnswersType.ACCURATE}>Развернутый ответ</option>}
                            {question.ans_type == 1 ? 
                                <option selected={true} className='select_item' value={AnswersType.SELECT}>Один верный ответ</option>
                            :   <option className='select_item' value={AnswersType.SELECT}>Один верный ответ</option>}
                            {question.ans_type == 2 ? 
                                <option selected={true} className='select_item' value={AnswersType.MULTISELECT}>Несколько верных ответов</option>
                            :   <option className='select_item' value={AnswersType.MULTISELECT}>Несколько верных ответов</option>}
                        </select>
                        <EditText question={question} task={this.task} up={this.update}/>
                        <ul>
                            {question.answers && question.answers.map((answer, index) =>
                            <li key={index}>
                                {answer.text}
                                <EditAnsw question={question} answ={answer} task={this.task} up={this.update}/>
                                <button 
                                    className={!answer.is_correct ? 'button3_2' : 'button3'}
                                    onClick={() => {
                                        let tasksService = this.context;
                                        answer.is_correct = false;
                                        tasksService.tasksService.editTask(this.task);
                                        this.forceUpdate(); }
                                }>
                                    Ответ неверный
                                </button>
                                <button 
                                    className={answer.is_correct ? 'button3_2' : 'button3'}
                                        onClick={() => {
                                            let tasksService = this.context;
                                            answer.is_correct = true;
                                            tasksService.tasksService.editTask(this.task);
                                            this.forceUpdate(); }
                                }>
                                    Ответ верный
                                </button>
                                <div>
                                    <button className='button4' onClick={this.deleteAnsw.bind(null, question, answer)}>Удалить ответ</button>
                                </div>
                            </li>)}
                            <button className='button4' onClick={() => this.setState({pushAnswValue: !this.state.pushAnswValue})}>
                                {!this.state.pushValue ? "+Добавить ответ" : "Закрыть"}
                            </button>
                            <this.Rendering2 value={this.state.pushAnswValue} question={question}/>
                        </ul>
                        
                        <button className='button4' onClick={this.deleteQuestion.bind(null, question.id)}>Удалить вопрос</button>
                    </li>)}
                </ul>
                <button className='button4' onClick={() => this.setState({pushValue: !this.state.pushValue})}>
                        {!this.state.pushValue ? "+Добавить вопрос" : "Закрыть"}
                </button>
                <this.Rendering value={this.state.pushValue}/>
            </div>
        );
    }
}

export default Edition;