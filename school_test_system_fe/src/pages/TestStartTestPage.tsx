import {FunctionComponent, useContext, useEffect, useState} from "react";
import * as React from "react";
import {ApiServices} from "../contexts/ApiStore";
import {Subject} from "../models/Tasks";
import {Grade} from "../models/Tasks";
import CreateSubject from "./CreateSubject";
import { Link, useParams } from 'react-router-dom';
import { StudentTest, Variant } from "../models/Tests";
import { StringDecoder } from "string_decoder";
import { stringify } from "querystring";

interface TestStartTestPageProps {
}

interface ParamTypes {
    param: string;
}

export const TestStartTestPage: FunctionComponent<TestStartTestPageProps> = () => {

    const {tasksService} = useContext(ApiServices);
    const [student_test, setStudentTests] = useState<StudentTest|null>(null);
    const {param} = useParams<ParamTypes>();

    useEffect(() => {
        tasksService.getStudentTestsById(parseInt(param)).then((data: StudentTest) => {
            setStudentTests(data);
        },)
    }, [tasksService]);

    const generate = async () => {
        let data = {
            student_test: student_test,
            is_control: true,
        }
        let test = await tasksService.generateVariant(data);
        window.location.replace("/tests/process/" + test.id);
    }

    const generateTraining = async () => {
        let data = {
            student_test: student_test,
            is_control: false,
        }
        let test = await tasksService.generateVariant(data);
        window.location.replace("/tests/process/" + test.id);
    }

    if (student_test){
        return (
            <div>
                <header className="header">{student_test.test.title}</header>
                <div className='list13' key={student_test.id}>
                    <br></br>
                    <br></br>
                    <div>
                        Дeдлайн: {student_test.deadline}
                    </div>
                    <br></br>
                    <div>Количество попыток: {student_test.num_attemps}</div>
                    <br></br>
                    <button className='button4' onClick={generate}>Начать тест</button>
                    {student_test.training_starttime ? 
                        <button className='button4' onClick={generateTraining}>Начать тренировку</button>
                    : null }
                </div>
            </div>
        );
    }
    return (<div></div>)
};

