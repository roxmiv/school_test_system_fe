import React from "react";
import { ApiServices } from "../contexts/ApiStore";
import { Question, Task } from "../models/Tasks";

class EditText extends React.Component<any, any> {
    static contextType = ApiServices;
    task: Task;

    constructor(props: any) {
        super(props);
        this.task = props.task;
        this.state = {
            value: false,
            question: props.question.text,
        }
    }

    edit = () => {
        if (this.task) {
            let tasksService = this.context;
            let ind = this.task.questions.findIndex((elem: Question) => elem === this.props.question);
            this.task.questions[ind].text = this.state.question;
            tasksService.tasksService.editTask(this.task);
            this.props.up();
        } else {
            this.props.edit(this.props.question.id, this.state.question);
        }
    }

    render () {
    return (
        <div>
            <button className='button4' onClick={() => this.setState({value: !this.state.value})}>Редактировать</button>
            {this.state.value ? 
                <>
                    <input
                        type="text"
                        value={this.state.question}
                        onChange={e => this.setState({question: e.target.value})}
                    />
                    <button className='button4_2' onClick={this.edit}>Сохранить</button>
                </>
            : null }
        </div>
    );
    }
}

export default EditText;