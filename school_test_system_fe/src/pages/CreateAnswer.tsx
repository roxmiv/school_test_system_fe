import React, { Component } from 'react';
import {ApiServices} from "../contexts/ApiStore";
import { Question, AnswersType, QuestionAnswer } from '../models/Tasks';

class CreateAnswer extends React.Component<any, any> {
    static contextType = ApiServices;
    inputText: React.RefObject<HTMLTextAreaElement>;
    answer: QuestionAnswer;

    constructor(props: any){
        super(props);
        this.inputText = React.createRef();
        this.answer = new QuestionAnswer();
        this.answer.is_correct = false;
        this.сreateAnswer = this.сreateAnswer.bind(this);
    }

    сreateAnswer = () => {
        if (this.inputText.current != null) {
            let ans = new QuestionAnswer;
            ans.text = this.inputText.current.value;
            ans.is_correct = this.answer.is_correct;
            this.props.func(ans, this.props.quest);
        }
    }

    render() {
        return(
            <div>
                <textarea className='input4' ref={this.inputText} placeholder='Введите ответ'></textarea>
                <button 
                    className={!this.answer.is_correct ? 'button3_2' : 'button3'}
                    onClick={() => {
                            this.answer.is_correct = false;
                            this.forceUpdate(); }
                    }>
                        Ответ неверный
                </button>
                <button 
                    className={this.answer.is_correct ? 'button3_2' : 'button3'}
                    onClick={() => {
                        this.answer.is_correct = true;
                        this.forceUpdate(); }
                }>
                        Ответ верный
                </button>
                <div>
                    <button className='button6' onClick={() => this.сreateAnswer()}>Создать ответ</button>
                </div>
                <br></br>
                <br></br>
                <br></br>
            </div>
        );
    }
} 

export default CreateAnswer;