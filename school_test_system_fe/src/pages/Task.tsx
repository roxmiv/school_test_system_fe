import {FunctionComponent, useContext, useEffect, useState} from "react";
import * as React from "react";
import {ApiServices} from "../contexts/ApiStore";
import {Question, Task} from "../models/Tasks";
import { useParams } from 'react-router-dom';
import { MainUser } from "../models/users_auth";

interface TaskPageInt {
}

interface ParamTypes {
    param: string;
}

export const TaskPage: FunctionComponent<TaskPageInt> = () => {
    const {tasksService} = useContext(ApiServices);
    const {param} = useParams<ParamTypes>();
    const [task, setTask] = useState<Task|null>(null);
    const [user, setUser] = useState<MainUser|null>(null);
    const [state, setState] = useState<boolean>(false);

    useEffect(() => {
        tasksService.getTask(parseInt(param)).then((data: Task) => {
            setTask(data);
        },)
    }, [tasksService]);

    useEffect(() => {
        tasksService.getUser().then((data: MainUser) => {
            setUser(data);
        },)
    }, [tasksService]);

    const pushQuestion = (props: Question) => {
        if (task) {
            task.questions.push(props);
            tasksService.editTask(task);
            window.location.reload();
        }
    }

    const replace = () => {
        if (task) {
            window.location.replace("/edit_task/" + task.id);
        }
    }
    if (user) {
        return (
            <div>
                {/* {user ? */}
                <div>
                    <div className='boldtext'>
                        {task?.text}
                    </div>    
                    <div className='list11'>
                        <>{(task?.is_control && task?.is_training) ? 'Задание контрольное, тренировочное' : null}</>
                        <>{(task?.is_control && !task?.is_training) ? 'Задание контрольное': null }</>
                        <>{(!task?.is_control && task?.is_training) ? 'Задание тренировочное': null }</>
                    </div>
                    <ul>
                        {task?.questions && task?.questions.map((question, index) =>
                        <li className='list11' key={question.id}>
                            {question.text}
                            <ul>
                            {question.ans_type != 0 ? 
                                <div>
                                {question.answers && question.answers.map((answer, index) =>
                                    <li key={answer.id}>
                                        {answer.text}
                                    </li>)}
                                </div>
                            : null }
                            </ul>
                        </li>)}
                    </ul>
                    <button className='button4' onClick={replace}>Редактировать задание</button>
                </div>
            {/* : <header className="header_home">Чтобы просматривать задания, авторизируйтесь.</header>} */}
            </div>
        );
    } else {
        return (
            <div>
                <header className="header_home">Чтобы просматривать задания, авторизируйтесь.</header>
            </div>
        );
    }
};
