import React, { Component } from 'react';
import { Subject } from '../models/Tasks';
import {ApiServices} from "../contexts/ApiStore";
import { useParams } from 'react-router-dom';
import {Grade} from "../models/Tasks";

interface ParamTypes {
    id: string;
}

class CreateSubject extends React.Component<ParamTypes> {

    static contextType = ApiServices
    context!: React.ContextType<typeof ApiServices>
    inputText: React.RefObject<HTMLTextAreaElement>;

    constructor(props: any){
        super(props);
        this.inputText = React.createRef();
    }

    pushSubject = async () => {
        if (this.inputText.current != null) {
            let subject = this.inputText.current.value;
            let tasksService = this.context;
            let sub = new Subject();
            console.log(sub);
            sub.grade = await tasksService.tasksService.getGradeName(parseInt(this.props.id));
            sub.name = subject;
            await tasksService.tasksService.createSubject(sub);
            // window.location.reload();
        }
    }

    render() {
        return(
            <div>
                <textarea className='input' ref={this.inputText} placeholder='Введите предмет'></textarea>
                <button className='button2' onClick={this.pushSubject}>Создать предмет</button>
            </div>
        );
    }
} 

export default CreateSubject;