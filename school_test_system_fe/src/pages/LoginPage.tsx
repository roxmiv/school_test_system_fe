import React, { Component } from 'react';
import {ApiServices} from "../contexts/ApiStore";
import "../App.css";

class LoginPage extends React.Component {

    static contextType = ApiServices;

    login: React.RefObject<HTMLTextAreaElement>;
    password: React.RefObject<HTMLTextAreaElement>;

    constructor(props: any){
        super(props);
        this.login = React.createRef();
        this.password = React.createRef();
    }

    Login = async () => {
        if (this.login.current != null &&
            this.password.current != null) 
        {
            let Login = this.login.current.value;
            let Password = this.password.current.value;
            let tasksService = this.context;
            let data = {
                username: Login,
                password: Password,
            }
            if (await tasksService.tasksService.login(data)) {
                let user = await tasksService.tasksService.getUser();
                console.log(user);
                window.location.replace("/home");
            }
        }
    }

    render() {
        return(
            <div>
            <div>
                <textarea className='input' ref={this.login} placeholder='Введите логин'></textarea>
            </div>
            <div>
                <textarea className='input' ref={this.password} placeholder='Введите пароль'></textarea>
            </div>
            <div>
                <button className='button' onClick={this.Login}>{"Войти"}</button>
            </div>
            </div>
        );
    }
} 

export default LoginPage;