import {FunctionComponent, useContext, useEffect, useState} from "react";
import * as React from "react";
import {ApiServices} from "../contexts/ApiStore";
import {Subject} from "../models/Tasks";
import {MainUser} from "../models/users_auth";
import CreateSubject from "./CreateSubject";
import { Link, useParams } from 'react-router-dom';
import "../App.css";

interface SubjectsListPageProps {
}

interface ParamTypes {
    param: string;
}

function Rendering(props: {value: boolean, param: string}) {
    const value = props.value;
    const param = props.param;
    if (value) {
        return <CreateSubject id={param}/>
    } else {
        return null;
    }
}

export const SubjectsListPage: FunctionComponent<SubjectsListPageProps> = () => {
    const {tasksService} = useContext(ApiServices);
    const [subjects, setSubjects] = useState<Subject[]>([]);
    const {param} = useParams<ParamTypes>();
    const [value, setValue] = useState(false);
    const [user, setUser] = useState<MainUser|null>(null);
    const [adminBool, setAdminBool] = useState(false);

    useEffect(() => {
        tasksService.getSubjectsListByGrade(param).then((data: Subject[]) => {
            setSubjects(data);
        },)
    }, [tasksService]);

    useEffect(() => {
        tasksService.getUser().then((data: MainUser) => {
            setUser(data);
        },)
    }, [tasksService]);

    return (
        <div>
            <header className="header">Выберите предмет:</header>
            <ul className="links">
                {subjects && subjects.map((subject, index) =>
                <li key={subject.id}>
                    <Link className="list" to={'/tasks/grades/subjects/' + subject.id}>{subject.name}</Link>
                </li>)}
            </ul>
            {user?.is_school_admin ?
                <>
                    <button className="button" onClick={() => setValue(!value)}>
                        {!value ? "Создать предмет" : "Закрыть"}
                    </button>
                    <Rendering value={value} param={param}/>
                </> : null }
        </div>
    );
};