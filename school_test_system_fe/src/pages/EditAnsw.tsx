import React from "react";
import { ApiServices } from "../contexts/ApiStore";
import { AnswersType, Question, QuestionAnswer, Task } from "../models/Tasks";

class EditAnsw extends React.Component<any, any> {
    static contextType = ApiServices;
    task: Task;

    constructor(props: any) {
        super(props);
        this.task = props.task;
        this.state = {
            value: false,
            ans: props.answ.text,
        }
    }

    edit = () => {
        if (this.task) {
            let tasksService = this.context;
            let ind = this.task.questions.findIndex((elem: Question) => elem === this.props.question);
            let ind1 = this.task.questions[ind].answers.findIndex((elem: QuestionAnswer) => elem === this.props.answ);
            this.task.questions[ind].answers[ind1].text = this.state.ans;
            tasksService.tasksService.editTask(this.task);
            this.props.up();
        } else {
            let ind = this.props.question.answers.findIndex((elem: QuestionAnswer) => elem === this.props.answ);
            this.props.edit(this.props.question.id, ind, this.state.ans);
        }
    }

    render () {
    return (
        <div>
            <button className='button4' onClick={() => this.setState({value: !this.state.value})}>Редактировать</button>
            {this.state.value ? 
                <>
                    <input
                        type="text"
                        value={this.state.ans}
                        onChange={e => this.setState({ans: e.target.value})}
                    />
                    <button className='button4_2' onClick={this.edit}>Сохранить</button>
                </>
            : null }
        </div>
    );
    }
}

export default EditAnsw;