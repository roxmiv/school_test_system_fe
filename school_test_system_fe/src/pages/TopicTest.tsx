import React from 'react';
import {FunctionComponent, useContext, useEffect, useState} from "react";
import {MainUser} from "../models/users_auth";
import {ApiServices} from "../contexts/ApiStore";
import {Link, useParams} from 'react-router-dom';
import {Subject, Topic} from "../models/Tasks";

export const TopicTestPage: (props: any) => JSX.Element = (props : any) => {
    const {tasksService} = useContext(ApiServices);
    const [topics, setTopics] = useState<Topic[]|null>(null);
    const [savedTopics, setSavedTopics] = useState<Topic[]|null>(null);

    useEffect(() => {
        tasksService.getTopicsList(props.subject.id).then((data: Topic[]) => {
            setTopics(data);
            setSavedTopics(data);
        },)
    }, [tasksService]);

    const filterList = (e: any) => {
        if (savedTopics) {
            var filteredList = savedTopics.filter(function(item) {
                return item.name.toLowerCase().search(e.target.value.toLowerCase())!== -1;
            });
            setTopics(filteredList);
        }
    }

    return(
        <div>
            <br></br>
            <input placeholder="Поиск" onChange={filterList} />
            <br></br>
            <br></br>
            <ul className="links">
                {topics && topics.map((topic, index) =>
                    // <Link className="list" key={user.id} to={'/tasks/grades/' + user.id}>
                    <button key={index} className={props.func_check(topic) ? 'button3_2' : 'button3'}
                            onClick={() => {console.log(topic);props.func(topic);props.f();}}
                    >
                        {topic.name}
                    </button>
                )}
            </ul>
        </div>
    );
}