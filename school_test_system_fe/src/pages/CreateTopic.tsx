import React from 'react';
import { Subject, Topic } from '../models/Tasks';
import {ApiServices} from "../contexts/ApiStore";
import NewSubjects from './NewSubjects';
import Modal from './Modal';

interface ParamTypes {
    id: string;
}

class CreateTopic extends React.Component<any, any> {

    static contextType = ApiServices
    context!: React.ContextType<typeof ApiServices>
    inputText: React.RefObject<HTMLTextAreaElement>;
    taskService: { tasksService: any; };
    subjects: Subject[];
    a: any[];

    constructor(props: any){
        super(props);
        this.inputText = React.createRef();
        this.taskService = this.context;
        this.subjects = [];
        this.state = {
            count: 0,
            modalActive: false,
        }
        this.a = [...Array(this.state.count)];
    }

    pushSubject = async () => {
        if (this.inputText.current != null) {
            let topic = this.inputText.current.value;
            let tasksService = this.context;
            let topicObj = new Topic();
            topicObj.subjects = this.subjects;
            topicObj.subjects.push(await tasksService.tasksService.getSubjectName(parseInt(this.props.id)));
            topicObj.name = topic;
            console.log(topicObj);
            tasksService.tasksService.createTopic(topicObj);
            // window.location.reload();
        }
    }

    addCnt = async () => {
        await this.setState({count: this.state.count + 1});
        this.a = [...Array(this.state.count)];
        this.forceUpdate();
    }

    deleteGrade = async () => {
        await this.setState({count: this.state.count - 1});
        this.a = [...Array(this.state.count)];
        this.forceUpdate();
    }

    render() {
        return(
            <div>
                <button onClick={() => this.setState({modalActive: !this.state.modalActive})}>Назначить другому классу</button>
                <Modal active={this.state.modalActive} setActive={(props: any) => this.setState({modalActive: props})}>
                    <NewSubjects subs={this.subjects} id={parseInt(this.props.id)}/>
                </Modal>
                <textarea className='input' ref={this.inputText} placeholder='Введите тему'></textarea>
                <button className='button2' onClick={this.pushSubject}>Сохранить</button>
            </div>
        );
    }
} 

export default CreateTopic;