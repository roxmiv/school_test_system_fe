import {FunctionComponent, useContext, useEffect, useState} from "react";
import {ApiServices} from "../contexts/ApiStore";
import { Grade, Difficult, GradeDifficulty, Topic, Subject } from '../models/Tasks';
import "../App.css";
import "../Select.css";


function NewSubjects(props: any) {
    const {tasksService} = useContext(ApiServices);
    const [grades, setGrades] = useState<Grade[]>([]);
    const [subjects, setSubjects] = useState<Subject[]>([]);
    const [subj, setSubj] = useState<Subject|null>(null);
    let grade: Grade|null;
    let subject: Subject|null;

    useEffect(() => {
        tasksService.getGradesList().then((data: Grade[]) => {
            setGrades(data.filter(elem => elem.id != props.id));
        },)
    }, [tasksService]);

    const gradeChange = async (event: any) => {
        grade = await tasksService.getGradeName(event?.target.value);
        console.log(grade);
        if (grade) {
            tasksService.getSubjectsListByGrade(grade.id).then((data: Subject[]) => {
                setSubjects(data);
            },)
        }
    }

    const topicChange = async (event: any) => {
        let s = await tasksService.getSubjectName(event.target.value);
        await setSubj(s);
    }

    const appoint = async () => {
        props.subs.push(subj);
    }

    return(
        <div>
            <select className="select" onChange={gradeChange}>
                <option className='select_head'>Выберите класс</option>
                {grades && grades.map((grade, index) =>
                    <option key={index} className='select_item' value={grade.id}>{grade.name}</option>    
                )}
            </select>
            <select className="select" onChange={topicChange}>
                <option className='select_head'>Выберите предмет</option>
                {subjects && subjects.map((subject, index) =>
                    <option key={index} className='select_item' value={subject.id}>{subject.name}</option>    
                )}
            </select>
            <button onClick={appoint}>Назначить</button>
        </div>
    );
}

export default NewSubjects;
