export default function EndText() {
    return (
        <div>
            <div className="header_home">
                Тест окончен
            </div>
            <button className="button2_2" onClick={() => window.location.replace('/tests')}>Вернуться</button>
        </div>
    );
}