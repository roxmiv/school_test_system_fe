import {FunctionComponent, useContext, useEffect, useState} from "react";
import * as React from "react";
import {ApiServices} from "../contexts/ApiStore";
import {Task} from "../models/Tasks";
import Modal from "./Modal";
import CreateTask from "./CreateTask";
import { Link, useParams } from 'react-router-dom';
import {MainUser} from "../models/users_auth";
import '../App.css';

interface TasksListPageProps {
}

interface ParamTypes {
    param: string;
}

export const TasksListPage: FunctionComponent<TasksListPageProps> = () => {
    const {tasksService} = useContext(ApiServices);
    const {param} = useParams<ParamTypes>();
    const [tasks, setTasks] = useState<Task[]>([]);
    const [isModal, setModal] = React.useState(false);
    const [user, setUser] = useState<MainUser|null>(null);

    useEffect(() => {
        tasksService.getTasksList(param).then((data: Task[]) => {
            setTasks(data);
        },)
    }, [tasksService]);

    useEffect(() => {
        tasksService.getUser().then((data: MainUser) => {
            setUser(data);
        },)
    }, [tasksService]);

    return (
        <div>
            <header className="header">Выберите задание:</header>
            <ul className="links">
                {tasks && tasks.map((topic, index) =>
                <li key={topic.id}>
                    <Link className="list" to={'/tasks/grades/subjects/topics/tasks/' + topic.id}>{topic.text}</Link>
                </li>)}
            </ul>
        </div>
    );
};