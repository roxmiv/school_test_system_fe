import {FunctionComponent, RefObject, useContext, useEffect, useState} from "react";
import {ApiServices} from "../contexts/ApiStore";
import "../App.css";
import { MainUser } from "../models/users_auth";
import { useParams } from 'react-router-dom';
import CreateRoles from "./CreateRoles";
import React from "react";

interface UserInfoProps {
}

interface ParamTypes {
    param: string;
}

export const UserInfo: FunctionComponent<UserInfoProps> = () => {
    const {tasksService} = useContext(ApiServices);
    const [user, setUser] = useState<MainUser|null>(null);
    const [isTeacher, setTeacher] = useState<boolean>(false);
    const [isStudent, setStudent] = useState<boolean>(false);
    const [value, setValue] = useState<boolean>(false);
    const {param} = useParams<ParamTypes>();

    let inputText = React.createRef<HTMLInputElement>();
    
    useEffect(() => {
        tasksService.getUserById(param).then((data: MainUser) => {
            setUser(data);
        },)
    }, [tasksService]); 

    const handleChange = (event: any) => {
        if (event.target.value == "teacher") {
            setTeacher(true);
            setStudent(false);
        } else if (event.target.value == "student"){
            setStudent(true);
            setTeacher(false);
        }
    }

    const onclick = async () => {
        let data = {
            main_user: user,
        }
        console.log(user);
        if (isTeacher) {
            await tasksService.createTeacher(data);
            window.location.reload();
        }
        if (isStudent) {
            await tasksService.createStudent(data);
            window.location.reload();
        }
        console.log(user);
    }

    const changePassword = () => {
        if (user && inputText.current) {
            let data = {
                username: user.username,
                new_password: inputText.current.value,
            }
            tasksService.changePassword(data);
        }
    }

    return (
        <div>
            {user ? 
                <div>
                    <div>Имя: {user.first_name}</div>
                    <div>Фамилия: {user.last_name}</div>
                    <div>Логин: {user.username}</div>
                    <div>Роль: {user.is_school_admin ? "Администратор" : 
                                user.is_teacher ? "Учитель" : 
                                user.is_student ? "Ученик" : "Нет"}</div>
                    <div>
                        <select className='select' onChange={handleChange}>
                        <option className='select_head'>Выберите роль пользователя</option>
                        <option className='select_item' value={"teacher"}>Учитель</option>
                        <option className='select_item' value={"student"}>Ученик</option>
                        </select>
                        <button onClick={onclick}>Назначить</button>
                    </div>
                    <button onClick={async () => {await setValue(!value);}}>Поменять пользователю пароль</button>
                    <div>
                        {value ? 
                            <div>
                                <input ref={inputText}/>
                                <button onClick={changePassword}>Поменять</button> 
                            </div>
                        : null}                       
                    </div>
                </div>
            : <div>Пользователь не найден</div>}

        </div>
    );
};