import React, { Component } from 'react';
import { Task, Question, GradeDifficulty, Topic, AnswersType, QuestionAnswer } from '../models/Tasks';
import {ApiServices} from "../contexts/ApiStore";
import CreateQuestion from "./CreateQuestion";
import NewDifficulties from "./NewDifficulties";
import "../App.css";
import "../Select.css";
import GradeAppoint from './GradeAppoint';
import EditAnsw from './EditAnsw';
import EditText from './EditText';
import CreateAnswer from './CreateAnswer';
import Modal from './Modal';

class CreateAccurateAnsw extends React.Component<any, any> {
    static contextType = ApiServices;
    inputText: React.RefObject<HTMLTextAreaElement>;
    answer: QuestionAnswer;

    constructor(props: any){
        super(props);
        this.inputText = React.createRef();
        this.answer = new QuestionAnswer();
        this.answer.is_correct = false;
        this.сreateAnswer = this.сreateAnswer.bind(this);
    }

    сreateAnswer = () => {
        if (this.inputText.current != null) {
            let ans = new QuestionAnswer;
            ans.text = this.inputText.current.value;
            ans.is_correct = true;
            this.props.func(ans, this.props.quest);
        }
    }
    
    render() {
        return(
            <div>
                <textarea className='input2' ref={this.inputText} placeholder='Введите ответ'></textarea>
                <div>
                    <button className='button3' onClick={() => this.сreateAnswer()}>Создать ответ</button>
                </div>
                <br></br>
            </div>
        );
    }
}

class CreateTask extends React.Component<any, any> {

    static contextType = ApiServices;
    context!: React.ContextType<typeof ApiServices>
    task: Task;
    questions: Question[];
    inputText: React.RefObject<HTMLTextAreaElement>;
      
    constructor(props: any){
        super(props);
        this.inputText = React.createRef();
        this.task = new Task();
        this.task.questions = new Array();
        this.task.grade_difficulty = new Array();
        this.task.is_control = false;
        this.task.is_training = false;
        this.task.is_control = false;
        this.task.is_training = false;
        this.state = {
            value: false,
            value1: false,
            count: 0,
            newDifficulties: [],
            modalActive: false,
        };
        this.questions = new Array();
        this.questions = [];
        this.pushQuestion = this.pushQuestion.bind(this);
        this.pushDifficulty = this.pushDifficulty.bind(this);
        this.Rendering = this.Rendering.bind(this);
        this.pushTask = this.pushTask.bind(this);
    }

    OnClick = () => {
        console.log(this.state.count);
        this.setState(({count} : any) => ({
            count: count + 1,
        }));
    }

    pushQuestion = (props: Question) => {
        this.questions.push(props);
        this.task.questions.push(props);
        this.forceUpdate();
    }

    pushDifficulty = (props: GradeDifficulty) => {
        this.task.grade_difficulty.push(props);
        console.log(this.task);
    }
    
    letGrade = (props: Topic) => {
        this.task.topic = props;
        console.log(this.task);
    }

    Rendering(props: {value: boolean}) {
        const value = props.value;
        if (value) {
            return <CreateQuestion func={this.pushQuestion} pos={this.task.questions.length}/>
        } else {
            return null;
        }
    }

    pushTask = async () => {
        let tasksService = this.context;
        let text = '';
        if (this.inputText.current) {
            text = this.inputText.current.value;
        }
        this.task.text = text;
        this.task.author = await tasksService.tasksService.getUser();
        console.log(this.task);
        tasksService.tasksService.createTask(this.task);
        this.inputText = React.createRef();
        this.task = new Task();
        this.task.questions = new Array();
        this.task.grade_difficulty = new Array();
        this.task.is_control = false;
        this.task.is_training = false;
        this.questions = new Array();
        // window.location.reload();
        window.location.reload();
    }

    editAnsw = (question_id: number, id_answ: number, str: string) => {
        let ind = this.task.questions.findIndex(el => el.id == question_id);
        this.task.questions[ind].answers[id_answ].text = str;
        this.forceUpdate();
    }

    editQuestText = (question_id: number, str: string) => {
        let ind = this.task.questions.findIndex((elem: Question) => elem.id === question_id);
        this.task.questions[ind].text = str;
        this.forceUpdate();
    }

    addNewAnsw = (answer: QuestionAnswer, quest: Question) => {
        quest.answers.push(answer);
        this.forceUpdate();
    }

    editAccurateAnsw = (answer: QuestionAnswer, quest: Question) => {
        quest.answers[0] = answer;
        this.forceUpdate();
    }

    taskGrade = (props: any) => {
        if (this.task.grade_difficulty.length == 0) {
            this.task.grade_difficulty.push(props);
            console.log(this.task);
        } else {
            this.task.grade_difficulty[0] = props;
            console.log(this.task);
        }
    }

    render() {
        return(
            <div>
                <header className="header">Создание задания</header>
                <br></br>
                <GradeAppoint func={this.letGrade} taskGrade={this.taskGrade}/>
                <div>
                    <button className='button5' onClick={() => this.setState({modalActive: !this.state.modalActive})}>Добавить другой класс</button>
                    <Modal active={this.state.modalActive} setActive={(props: any) => this.setState({modalActive: props})}>
                        <NewDifficulties push={this.pushDifficulty}/>
                    </Modal>
                </div>
                <br></br>
                <br></br>
                <textarea className='input2_0' ref={this.inputText} placeholder='Введите текст задания'></textarea>
                <div> 
                    <button 
                        className={this.task.is_control ? 'button3_2' : 'button3'}
                        onClick={() => { this.task.is_control = !this.task.is_control; 
                                         this.forceUpdate(); }}
                    >
                            Задание контрольное
                    </button>
                    <button
                        className={this.task.is_training ? 'button3_2' : 'button3'}
                        onClick={() => { this.task.is_training = !this.task.is_training;
                                         this.forceUpdate(); }}
                    >
                            Задание тренировочное
                    </button>
                </div>
                <ul>
                    {this.questions && this.questions.map((question, index1) =>
                    <li className='list11' key={index1}>
                        {question.text}
                        <EditText question={question} task={null} edit={this.editQuestText}/>
                        <select defaultValue={question.ans_type} className='select' onChange={(event: any) => {question.ans_type = event; this.forceUpdate();}}>
                            <option className='select_head'>Выберите тип вопроса</option>
                            <option className='select_item' value={AnswersType.ACCURATE}>Развернутый ответ</option>
                            <option className='select_item' value={AnswersType.SELECT}>Один верный ответ</option>
                            <option  className='select_item' value={AnswersType.MULTISELECT}>Несколько верных ответов</option>
                        </select>
                        <ul>
                        {question.answers && question.answers.map((answer, index2) =>
                            <li key={index2}>
                                {answer.text}
                                <EditAnsw question={question} task={null} answ={answer} edit={this.editAnsw}/>
                                {question.ans_type != 0 ?
                                <div>
                                    <button 
                                        className={!answer.is_correct ? 'button3_2' : 'button3'}
                                        onClick={() => {
                                            answer.is_correct = false;
                                            this.forceUpdate(); }
                                        }>
                                        Ответ неверный
                                    </button>
                                    <button 
                                        className={answer.is_correct ? 'button3_2' : 'button3'}
                                        onClick={() => {
                                            answer.is_correct = true;
                                            this.forceUpdate(); }
                                        }>
                                        Ответ верный
                                    </button>
                                </div>
                                : null}
                                <button className='button4' onClick={() => {
                                    question.answers = question.answers.filter(el => el != answer);
                                    this.forceUpdate();
                                }}>
                                    Удалить ответ
                                </button>
                            </li>)}
                        </ul>
                        {question?.ans_type != 0 ?
                        <div>
                            <button className='button4' onClick={() => this.setState({value1: !this.state.value1})}>
                                {!this.state.value1 ? "+Добавить ответ" : "Закрыть"}
                            </button>
                            {this.state.value1 ?
                            <CreateAnswer func={this.addNewAnsw} quest={question}/>
                            : null}
                        </div>
                        : null }
                        {question?.ans_type == 0 ?
                        <div>
                            <button className='button4' onClick={() => this.setState({value1: !this.state.value1})}>
                                {!this.state.value1 ? "+Добавить ответ" : "Закрыть"}
                            </button>
                            {this.state.value1 ?
                                <CreateAccurateAnsw func={this.editAccurateAnsw} quest={question}/>
                            : null }
                        </div>
                        : null }
                        <hr></hr>
                        <button className='button4' onClick={() => {
                            this.questions = this.questions.filter(el => el != question);
                            this.forceUpdate();
                        }}>
                            Удалить вопрос
                        </button>
                    </li>)}
                </ul>
                <div>
                    <button className='button4' onClick={() => this.setState({value: !this.state.value})}>
                        {!this.state.value ? "+Добавить вопрос" : "Закрыть"}
                    </button>
                    <this.Rendering value={this.state.value}/>
                </div>
                <button className='button5' onClick={this.pushTask}>Создать задание</button>
            </div>
        );
    }
} 

export default CreateTask;