import * as React from 'react';
import {FunctionComponent, useContext, useEffect, useState} from "react";
import {ApiServices} from "../contexts/ApiStore";
import "../App.css";
import { MainUser } from '../models/users_auth';

interface MainPageProps {

}

export const MainPage: FunctionComponent<MainPageProps> = () => {
    
    const {tasksService} = useContext(ApiServices);
    const [main_user, setMainUser] = useState<MainUser|null>(null);
    const [value, setValue] = useState(false);

    useEffect(() => {
        tasksService.getUser().then((data: MainUser) => {
            setMainUser(data);
        },)
    }, [tasksService]);
    
    if (main_user) {
        return (
            <header className="header_home">
                Здравствуйте, {main_user.first_name}!
            </header>
        );
    }
    
    return (
        <header className="header_home">
            Авторизуйтесь, пожалуйста!
        </header>
    );
};
