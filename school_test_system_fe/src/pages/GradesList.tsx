import {FunctionComponent, useContext, useEffect, useState} from "react";
import {ApiServices} from "../contexts/ApiStore";
import {Grade} from "../models/Tasks";
import CreateGrade from "./CreateGrade";
import { Link } from 'react-router-dom';
import {MainUser} from "../models/users_auth";
import "../App.css";

interface GradesListPageProps {
}

function Rendering(props: any) {
    const value = props.value;
    if (value) {
        return <CreateGrade />
    } else {
        return null;
    }
}

export const GradesListPage: FunctionComponent<GradesListPageProps> = () => {
    const {tasksService} = useContext(ApiServices);
    const [grades, setGrades] = useState<Grade[]>([]);
    const [adminBool, setAdminBool] = useState(false);
    const [value, setValue] = useState(false);
    const [user, setUser] = useState<MainUser|null>(null);

    useEffect(() => {
        tasksService.getGradesList().then((data: Grade[]) => {
            setGrades(data);
        },)
    }, [tasksService]);

    useEffect(() => {
        tasksService.getUser().then((data: MainUser) => {
            setUser(data);
        },)
    }, [tasksService]);

    return (
        <div>
        <header className="header">Выберите класс:</header>
        <ul className="links">
            {grades && grades.map((grade, index) =>
                <Link className="list" key={grade.id} to={'/tasks/grades/' + grade.id}>
                    <li>{grade.name}</li>
                </Link>            
            )}
        </ul>
        {user?.is_school_admin ?
            <>
                <button className="button" onClick={() => setValue(!value)}>
                    {!value ? "Создать класс" : "Закрыть"}
                </button>
                <Rendering value={value} />
            </> : null}
        </div>
    );
};