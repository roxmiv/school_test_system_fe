import {FunctionComponent, useContext, useEffect, useState} from "react";
import {ApiServices} from "../contexts/ApiStore";
import { Grade, Difficult, GradeDifficulty, Topic, Subject } from '../models/Tasks';
import "../App.css";
import "../Select.css";



interface NewDifficulties {
}

function NewDifficulties(props: any) {
    const {tasksService} = useContext(ApiServices);
    const [grades, setGrades] = useState<Grade[]>([]);
    const [difficulties, setDifficulties] = useState<GradeDifficulty[]>([]);
    const [difficulty, setDifficulty] = useState<GradeDifficulty|null>(null);
    let grade: Grade|null;
    let subject: Subject|null;

    useEffect(() => {
        tasksService.getGradesList().then((data: Grade[]) => {
            setGrades(data);
        },)
    }, [tasksService]);

    const gradeChange = async (event: any) => {
        grade = await tasksService.getGradeName(event?.target.value);
        console.log(grade);
        if (grade) {
            tasksService.getDifficultiesList(grade.id).then((data: GradeDifficulty[]) => {
                setDifficulties(data);
            },)
        }
    }

    const taskDifficulty = async (event: any) => {
        await setDifficulty(difficulties[event.target.value]);
    }

    const appoint = async () => {
        props.push(difficulty);
    }

    return(
        <div>
            <select className="select" onChange={gradeChange}>
                <option className='select_head'>Выберите класс</option>
                {grades && grades.map((grade, index) =>
                    <option key={index} className='select_item' value={grade.id}>{grade.name}</option>    
                )}
            </select>
            <select className="select" onChange={taskDifficulty}>
                <option className='select_head'>Выберите сложность задания</option>
                <option className='select_item' value={Difficult.EASY}>Просто</option>
                <option className='select_item' value={Difficult.MEDIUM}>Средне</option>
                <option className='select_item' value={Difficult.HARD}>Сложно</option>
            </select>
            <button className='button4' onClick={appoint}>Назначить</button>
        </div>
    );
}

export default NewDifficulties;
