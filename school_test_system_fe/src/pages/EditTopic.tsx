import React from "react";
import { ApiServices } from "../contexts/ApiStore";
import { Question, Task, Topic } from "../models/Tasks";

class EditTopic extends React.Component<any, any> {
    static contextType = ApiServices;
    topic: Topic;

    constructor(props: any) {
        super(props);
        this.topic = props.topic;
        this.state = {
            value: false,
            topicText: props.topic.name,
        }
    }

    edit = () => {
        let tasksService = this.context;
        let t = this.topic;
        t.name = this.state.topicText;
        tasksService.tasksService.editTopic(t);
        window.location.reload();
    }

    render () {
    return (
        <div>
            {this.state.value ?
                <button className='button7' onClick={() => this.setState({value: !this.state.value})}>Закрыть</button>
                : <button className='button7' onClick={() => this.setState({value: !this.state.value})}>Редактировать</button>
            }
            {this.state.value ? 
                <>
                    <input
                        className="edition"
                        type="text"
                        value={this.state.topicText}
                        onChange={e => this.setState({topicText: e.target.value})}
                    />
                    <button className='button8' onClick={this.edit}>Сохранить</button>
                </>
            : null }
        </div>
    );
    }
}

export default EditTopic;