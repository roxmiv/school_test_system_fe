import React from "react";
import {ApiServices} from "../contexts/ApiStore";
import {Difficult, Grade, GradeDifficulty, Question, Subject, Task, Topic} from "../models/Tasks";
import {Duration, Test, TestTask} from "../models/Tests";
import CreateTestTask from "./CreateTestTask";
import TextareaAutosize from 'react-textarea-autosize';
import AppointTest from "./TestAppoint";
import NewDifficulties from "./NewDifficulties";


class CreateTest extends React.Component<any, any> {
    static contextType = ApiServices;
    inputText: React.RefObject<HTMLTextAreaElement>;
    inputText1: React.RefObject<HTMLInputElement>;
    inputText2: React.RefObject<HTMLInputElement>;
    inputText3: React.RefObject<HTMLInputElement>;
    test: Test;
    test_tasks: TestTask[];
    context!: React.ContextType<typeof ApiServices>
    duration!:Duration;
    grade: Grade;

    constructor(props: any) {
        super(props);
        this.inputText = React.createRef();
        this.inputText1 = React.createRef();
        this.inputText2 = React.createRef();
        this.inputText3 = React.createRef();
        this.test = new Test();
        this.test.test_tasks = [];
        this.test.grade_difficulty = new GradeDifficulty();
        this.test.training_possible = false;
        this.test.duration = new Duration();
        this.test.duration.days = 1;
        this.test.duration.hours = 1;
        this.test.duration.minutes = 1;
        this.grade = new Grade();

        this.state = {
            value: false,
            count: 0,
            newDifficulties: [],
        };
        this.test_tasks = [];
        this.pushTest = this.pushTest.bind(this);
        this.pushTestTask = this.pushTestTask.bind(this);
        this.pushDifficulty = this.pushDifficulty.bind(this);
        this.Rendering = this.Rendering.bind(this);
    }

    OnClick = () => {
        this.setState(({count} : any) => ({
            count: count + 1,
        }));
    }

    pushTestTask = (props: TestTask) => {
        console.log(props);
        this.test_tasks.push(props);
        this.test.test_tasks.push(props);
        this.forceUpdate();
    }

    pushDifficulty = (props: GradeDifficulty) => {
        this.test.grade_difficulty = props;
    }

    letGrade = (props: Subject) => {
        this.test.subject = props;
    }

    pushTest = async () => {
        let tasksService = this.context;
        let text = '';
        if (this.inputText.current) {
            text = this.inputText.current.value;
        }
        if (this.inputText1.current) {
            this.test.duration.days = parseInt(this.inputText1.current?.value)
        }
        if (this.inputText2.current) {
            this.test.duration.hours = parseInt(this.inputText2.current?.value)
        }
        if (this.inputText3.current) {
            this.test.duration.minutes = parseInt(this.inputText3.current?.value)
        }
        this.test.title = text;
        this.test.author = await tasksService.tasksService.getUser();
        console.log(this.test);
        await tasksService.tasksService.createTest(this.test);
        this.inputText = React.createRef();
        this.test = new Test();
        this.test.test_tasks = [];
        this.test.grade_difficulty = new GradeDifficulty();
        this.test.training_possible = false;
    }

    Rendering(props: {value: boolean}) {
        const value = props.value;
        if (value) {
            return <CreateTestTask func={this.pushTestTask} subject={this.test.subject}/>
        } else {
            return null;
        }
    }
    func = async () => {
        this.test.training_possible = !this.test.training_possible;
        this.forceUpdate();
    }


    render() {
        return(

            <div>
                <h3 style={{textAlign: "center"}}>Создание теста</h3>
                <br></br>
                <AppointTest func={this.letGrade} push={this.pushDifficulty}/>
                <div>
                    {[...Array(this.state.count)].map((elem, index) => <NewDifficulties key={index} push={this.pushDifficulty}/>)}
                    <div>
                        {
                            this.state.count > 0 ?
                                <button onClick={ () => this.setState(({count} : any) => ({
                                    count: count - 1,
                                }))}>Удалить</button> : null
                        }
                    </div>
                </div>
                <hr></hr>
                <div > Введите название теста:</div><TextareaAutosize ref={this.inputText} placeholder='Введите название теста(максимум 255 символов)'
                autoFocus style={{resize:"none", paddingInline:50, paddingTop:0, marginLeft:0}} maxLength={255} cols={45} rows={100}></TextareaAutosize>
                <br></br>
                <div>
                    <hr></hr>
                    <div style={{ borderTop: "2px solid #fff ", marginLeft: 20, marginRight: 20 }}></div>
                    <div> Сделать тест доступным для тренировки: <input type="checkbox" checked={this.test.training_possible ? true : false} onChange={this.func} size={32}/>
                    </div>
                    <hr></hr>
                </div>
                <ul>
                    {this.test_tasks && this.test_tasks.map((test_task, index1) =>
                        <li key={index1}>
                            {test_task.position + '.' + test_task.topic.name}
                        </li>)}
                </ul>
                <div>
                    <button className='button4' onClick={() => this.setState({value: !this.state.value})}>
                        {!this.state.value ? "+Добавить тестовое задание" : "Закрыть"}
                    </button>
                    <this.Rendering value={this.state.value}/>
                </div>
                <hr></hr>
                <div>
                    <h4>Длительность теста</h4>
                    <label>Введите количество дней:</label>
                    <input type={"number"} ref={this.inputText1}></input>
                    <div></div>
                    <label>Введите количество часов:</label>
                    <input type={"number"} ref={this.inputText2}></input>
                    <div></div>
                    <label>Введите количество минут:</label>
                    <input type={"number"} ref={this.inputText3}></input>
                </div>
                <hr></hr>
                <button className='button4' onClick={this.pushTest}>Создать тест</button>
            </div>
        );
    }

}

export default CreateTest;