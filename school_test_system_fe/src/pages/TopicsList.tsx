import {FunctionComponent, useContext, useEffect, useState} from "react";
import {ApiServices} from "../contexts/ApiStore";
import {Topic} from "../models/Tasks";
import CreateTopic from "./CreateTopic";
import AddTopic from "./AddTopic";
import { Link, useParams } from 'react-router-dom';
import {MainUser} from "../models/users_auth";
import EditTopic from "./EditTopic";
import Modal from "./Modal";

interface TopicsListPageProps {
}

interface ParamTypes {
    param: string;
}

function Rendering(props: {value: boolean, param: string}) {
    const value = props.value;
    const param = props.param;
    if (value) {
        return <CreateTopic id={param}/>
    } else {
        return null;
    }
}

function Rendering2(props: {value: boolean, param: string}) {
    const value = props.value;
    const param = props.param;
    if (value) {
        return <AddTopic id={param}/>
    } else {
        return null;
    }
}

export const TopicsListPage: FunctionComponent<TopicsListPageProps> = () => {
    const {tasksService} = useContext(ApiServices);
    const {param} = useParams<ParamTypes>();
    const [topics, setTopics] = useState<Topic[]>([]);
    const [topi, setTopic] = useState<string>("");
    const [value, setValue] = useState(false);
    const [value2, setValue2] = useState(false);
    const [value3, setValue3] = useState(false);
    const [user, setUser] = useState<MainUser|null>(null);

    useEffect(() => {
        tasksService.getTopicsList(param).then((data: Topic[]) => {
            setTopics(data);
        },)
    }, [tasksService]);

    useEffect(() => {
        tasksService.getUser().then((data: MainUser) => {
            setUser(data);
        },)
    }, [tasksService]);

    const edit = (id: number, text: string) => {
        let t = tasksService.getTopic(id);
        t.text = text;
        tasksService.editTopic(t);
        window.location.reload();
    }

    return (
        <div>
            <header className="header">Выберите тему:</header>
            <ul className="links">
                {topics && topics.map((topic, index) =>
                <li key={topic.id}>
                    <Link className="list" to={'/tasks/grades/subjects/topics/' + topic.id}>{topic.name}</Link>
                    {user?.is_teacher ? 
                        <EditTopic topic={topic}/>
                    : null}
                </li>)}
            </ul>
            {user?.is_teacher ? 
            <>
                <div>
                    {!value ? <button className="button" onClick={() => setValue(!value)}>Создать тему</button>
                            : <button className="button" onClick={() => setValue(!value)}>Закрыть</button>}
                </div>
                {!value2 ? <button className="button" onClick={() => setValue2(!value2)}>Выбрать существующую тему</button>
                         : <button className="button" onClick={() => setValue2(!value2)}>Закрыть</button>}
                <Rendering value={value} param={param}/>
                <Modal active={value2} setActive={setValue2}>
                    <AddTopic id={param}/>
                </Modal>
            </> : null }
        </div>
    );
};