import {FunctionComponent, useContext, useEffect, useState} from "react";
import * as React from "react";
import {ApiServices} from "../contexts/ApiStore";
import {StudentTest} from "../models/Tests";
import {Link, useParams} from 'react-router-dom';
import Modal from "./Modal";
import CreateTest from "./CreateTest";

interface StudentTestsListPageProps {
}

function Rendering(props: any) {
    const value = props.value;
    if (value) {
        return null;
    } else {
        return null;
    }
}

interface ParamTypes {
    param: string;
}
export const TestsTests: FunctionComponent<StudentTestsListPageProps> = () => {
    const {tasksService} = useContext(ApiServices);
    const [student_tests, setTests] = useState<StudentTest[]>([]);
    const [value, setValue] = useState(false);
    const [isModal, setModal] = React.useState(false);
    const {param} = useParams<ParamTypes>();

    useEffect(() => {
        tasksService.getStudentTestsList().then((data: StudentTest[]) => {
            setTests(data);
        },)
    }, [tasksService]);

    return (
        <div>
            <ul className='links'>
                <li><Link className='link_center' to="/tests/">Текущие тесты</Link></li>
                <li><Link className='link_center' to="/tests/completed_tests">Пройденные тесты</Link></li>
            </ul>
            {student_tests && student_tests.map((student_test, index) =>
                <ul className='list11' key={index}>
                    <li>
                        <Link to={'/tests/' + student_test.id}>{student_test.test?.title}</Link>
                        <br></br>
                        <br></br>
                        <div>
                            Начало: {student_test.starttime.toLocaleString()}
                        </div>
                        <br></br>
                        <div>
                            Дедлайн: {student_test.deadline.toLocaleString()}
                        </div>
                    </li>
                    <br></br>
                    <div>
                        <Link to={'/tests/teacher/create_test'}>
                            <button className="button4">Добавить тест</button>
                        </Link>
                    </div>
                </ul>
            )
            }
        </div>
    );
};

