import {FunctionComponent, useContext, useEffect, useState} from "react";
import {ApiServices} from "../contexts/ApiStore";
import { Grade, Difficult, GradeDifficulty, Topic, Subject } from '../models/Tasks';
import "../App.css";
import "../Select.css";

function AppointGrade(props: any) {
    const {tasksService} = useContext(ApiServices);
    const [grades, setGrades] = useState<Grade[]>([]);
    const [difficulties, setDifficulties] = useState<GradeDifficulty[]>([]);
    const [difficulty, setDifficulty] = useState<GradeDifficulty|null>(null);
    const [subjects, setSubjects] = useState<Subject[]>([]);
    const [topics, setTopics] = useState<Topic[]>([]);
    let grade: Grade|null;
    let subject: Subject|null;

    useEffect(() => {
        tasksService.getGradesList().then((data: Grade[]) => {
            setGrades(data);
        },)
    }, [tasksService]);

    const gradeChange = async (event: any) => {
        grade = await tasksService.getGradeName(event?.target.value);
        console.log(grade);
        if (grade) {
            tasksService.getSubjectsListByGrade(grade.id).then((data: Subject[]) => {
                setSubjects(data);
            },)
            tasksService.getDifficultiesList(grade.id).then((data: GradeDifficulty[]) => {
                setDifficulties(data);
            },)
        }
    }

    const SubjectChange = async (event: any) => {
        subject = await tasksService.getSubjectName(event?.target.value);
        if (subject) {
            tasksService.getTopicsList(subject.id).then((data: Topic[]) => {
                setTopics(data);
            },)
        }
        console.log(subject);
    }

    const topicChange = async (event: any) => {
        let topic = await tasksService.getTopic(event.target.value);
        props.func(topic);
    }

    const taskDifficulty = async (event: any) => {
        props.taskGrade(difficulties[event.target.value]);
    }

    const appoint = async () => {
        props.push(difficulty);
    }

    return(
        <div>
            <select className="select" onChange={gradeChange}>
                <option className='select_head'>Выберите класс</option>
                {grades && grades.map((grade, index) =>
                    <option key={index} className='select_item' value={grade.id}>{grade.name}</option>    
                )}
            </select>
            <select className="select" onChange={SubjectChange}>
                <option className='select_head'>Выберите предмет</option>
                {subjects && subjects.map((subject, index) =>
                    <option key={index} className='select_item' value={subject.id}>{subject.name}</option>    
                )}
            </select>
            <select className="select" onChange={topicChange}>
                <option className='select_head'>Выберите тему</option>
                {topics && topics.map((topic, index) =>
                    <option key={index} className='select_item' value={topic.id}>{topic.name}</option>    
                )}
            </select>
            <select className="select" onChange={taskDifficulty}>
                <option className='select_head'>Выберите сложность задания</option>
                <option className='select_item' value={Difficult.EASY}>Просто</option>
                <option className='select_item' value={Difficult.MEDIUM}>Средне</option>
                <option className='select_item' value={Difficult.HARD}>Сложно</option>
            </select>
            {/* <button className="button4" onClick={appoint}>Подтвердить</button> */}
        </div>
    );
}

export default AppointGrade;
