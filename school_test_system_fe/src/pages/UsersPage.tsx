import React from 'react';
import {FunctionComponent, useContext, useEffect, useState} from "react";
import {MainUser} from "../models/users_auth";
import {ApiServices} from "../contexts/ApiStore";
import { Link } from 'react-router-dom';

interface UsersPageProps {
}

export const UsersPage: FunctionComponent<UsersPageProps> = () => {
    const {tasksService} = useContext(ApiServices);
    const [users, setUsers] = useState<MainUser[]|null>(null);
    const [savedUsers, setSavedUsers] = useState<MainUser[]|null>(null);

    useEffect(() => {
        tasksService.getUsersList().then((data: MainUser[]) => {
            setUsers(data);
            setSavedUsers(data);
        },)
    }, [tasksService]);

    const filterList = (e: any) => {
        if (savedUsers) {
            var filteredList = savedUsers.filter(function(item) {
                return item.username.toLowerCase().search(e.target.value.toLowerCase())!== -1;
            });
            setUsers(filteredList);
        }
    }

    return(
        <div>
            <br></br>
            <input className='search' placeholder="Поиск" onChange={filterList} />
            <br></br>
            <br></br>
            <ul className='list13'>
                {users && users.map((user, index) =>
                    <Link key={user.id} to={'/users/user/' + user.id + '/'}>
                        <li>{user.username} ({user.first_name} {user.last_name})</li>
                    </Link>            
                )}
            </ul>
        </div>
    );
}