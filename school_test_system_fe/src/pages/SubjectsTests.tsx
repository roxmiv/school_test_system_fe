import {FunctionComponent, useContext, useEffect, useState} from "react";
import * as React from "react";
import {ApiServices} from "../contexts/ApiStore";
import {Subject} from "../models/Tasks";
import { Link, useParams } from 'react-router-dom';

interface SubjectsListPageProps {
}

interface ParamTypes {
    param: string;
}

export const SubjectsTests: FunctionComponent<SubjectsListPageProps> = () => {
    const {tasksService} = useContext(ApiServices);
    const [subjects, setSubjects] = useState<Subject[]>([]);
    const {param} = useParams<ParamTypes>();

    useEffect(() => {
        tasksService.getSubjectsListByGrade(param).then((data: Subject[]) => {
            setSubjects(data);
        },)
    }, [tasksService]);

    return (
        <div>
            <>Выберите предмет:</>
            <ul>
                {subjects && subjects.map((subject, index) =>
                <li key={subject.id}>
                    <Link to={'/tests/grades/subjects/' + subject.id}>{subject.name}</Link>
                </li>)}
            </ul>
        </div>
    );
};