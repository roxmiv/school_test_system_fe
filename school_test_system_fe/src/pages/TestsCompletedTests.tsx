import {FunctionComponent, useContext, useEffect, useState} from "react";
import * as React from "react";
import {ApiServices} from "../contexts/ApiStore";
import {Test, Variant} from "../models/Tests";
import { Link, useParams } from 'react-router-dom';

interface CompletedTestsListPageProps {
}

export const TestsCompletedTests: FunctionComponent<CompletedTestsListPageProps> = () => {
    const {tasksService} = useContext(ApiServices);
    const [completed_test, setCompletedTests] = useState<Variant[]>([]);

    useEffect(() => {
        tasksService.getVariantsList().then((data: Variant[]) => {
            setCompletedTests(data);
        },)
    }, [tasksService]);

    if (completed_test) {
        return (
            <div>
                <ul className='links'>
                    <li><Link className='link_center' to="/tests/">Текущие тесты</Link></li>
                    <li><Link className='link_center' to="/tests/completed_tests/">Пройденные тесты</Link></li>
                </ul>
                <ul>
                    {completed_test && completed_test.map((variant, index) =>
                        <div className='list14' key={variant.id}>
                            <Link key={variant.id} to={'/tests/completed_tests/' + variant.id}>
                                <li>{variant.student_test.test.title}</li>
                            </Link>
                            <ul>
                                Набрано баллов: {variant.score}
                            </ul>
                            <ul>
                                Всего: {variant.max_score}
                            </ul>
                        </div>
                    )}
                </ul>
            </div>
        );
    }
    return (
        <div></div>
    );
};

interface PrintCompletedTestPageProps {
    
}

interface ParamTypes {
    param: string;
}

export const PrintCompletedTest: FunctionComponent<PrintCompletedTestPageProps> = () => {
    const {tasksService} = useContext(ApiServices);
    const [variant, setVariant] = useState<Variant|null>(null);
    const {param} = useParams<ParamTypes>();

    useEffect(() => {
        tasksService.getVariant(parseInt(param)).then((data: Variant) => {
            setVariant(data);
        },)
    }, [param, tasksService]);

    if (variant){
        return (
            
            <div>
                {variant.variant_tasks && variant.variant_tasks.map((variant_task, index) =>
                <ul key={variant_task.id}>
                    {variant_task.position + '. ' + variant_task.mask_task.text}
                    {variant_task.mask_task.questions && variant_task.mask_task.questions.map((question, index) =>
                        <ul key={question.id}>
                            {question.position + 1 + '. ' + question.text}
                            {question.answers && question.answers.map((answer, index) =>
                            <ul key={answer.id}>
                                {(index + 1) + '. ' + answer.text}
                            </ul>
                            )}
                        </ul>)}
                </ul>)}
        </div>
        );
    }
    return (<div></div>)
}