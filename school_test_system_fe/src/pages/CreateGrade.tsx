import React, { Component } from 'react';
import { Grade } from '../models/Tasks';
import {ApiServices} from "../contexts/ApiStore";
import "../App.css";

class CreateGrade extends React.Component {

    static contextType = ApiServices
    context!: React.ContextType<typeof ApiServices>

    inputText: React.RefObject<HTMLTextAreaElement>;

    constructor(props: any){
        super(props);
        this.inputText = React.createRef();
    }

    pushGrade = () => {
        if (this.inputText.current != null) {
            let grade = this.inputText.current.value;
            let tasksService = this.context;
            let item = new Grade();
            item.name = grade;
            tasksService.tasksService.createGrade(item);
            console.log(item);
            window.location.reload();
        }
    }

    render() {
        return(
            <div>
                <textarea className='input' ref={this.inputText} placeholder='Введите класс'></textarea>
                <button className='button2' onClick={this.pushGrade}>Создать класс</button>
            </div>
        );
    }
} 

export default CreateGrade;