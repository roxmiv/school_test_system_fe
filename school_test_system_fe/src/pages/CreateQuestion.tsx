import React, { Component } from 'react';
import { Question, AnswersType, QuestionAnswer } from '../models/Tasks';
import {ApiServices} from "../contexts/ApiStore";
import CreateAnswer from "./CreateAnswer";
import "../App.css";
import "../Select.css";
import { runInThisContext } from 'vm';
import EditAnsw from './EditAnsw';

class CreateAccurateAnsw extends React.Component<any, any> {
    static contextType = ApiServices;
    inputText: React.RefObject<HTMLTextAreaElement>;
    answer: QuestionAnswer;

    constructor(props: any){
        super(props);
        this.inputText = React.createRef();
        this.answer = new QuestionAnswer();
        this.answer.is_correct = false;
        this.сreateAnswer = this.сreateAnswer.bind(this);
    }

    сreateAnswer = () => {
        if (this.inputText.current != null) {
            let ans = new QuestionAnswer;
            ans.text = this.inputText.current.value;
            ans.is_correct = true;
            this.props.func(ans, this.props.quest);
        }
    }
    
    render() {
        return(
            <div>
                <textarea className='input2' ref={this.inputText} placeholder='Введите ответ'></textarea>
                <div>
                    <button className='button3' onClick={() => this.сreateAnswer()}>Создать ответ</button>
                </div>
                <br></br>
            </div>
        );
    }
}

class SelectAnswType extends React.Component<any, any> {
    static contextType = ApiServices;

    constructor(props: any){
        super(props);
    }

    handleChange = (event: any) => {
        this.props.func(parseInt(event.target.value));
    }

    render () {
    return(
        <div>
            <select className='select' onChange={this.handleChange}>
                <option selected={true} value="" disabled hidden>Выберите тип вопроса</option>
                <option className='select_item' value={AnswersType.ACCURATE}>Развернутый ответ</option>
                <option className='select_item' value={AnswersType.MULTISELECT}>Несколько верных ответов</option>
                <option className='select_item' value={AnswersType.SELECT}>Один верный ответ</option>
            </select>
        </div>
    );
    }
}

class CreateQuestion extends React.Component<any, any> {
    static contextType = ApiServices;
    inputText: React.RefObject<HTMLTextAreaElement>;
    question: Question;

    constructor(props: any){
        super(props);
        this.inputText = React.createRef();
        this.state = {
            value: false,
            count: 0,
            // ansType: 1,
        };
        this.question = new Question();
        // this.question.ans_type = this.state.ansType;
        this.question.answers = [];
        this.handleChange = this.handleChange.bind(this);
        this.pushQuestion = this.pushQuestion.bind(this);
        this.Rendering = this.Rendering.bind(this);
    }

    handleChange(event: any) {
        this.question.ans_type = parseInt(event.target.value);
        // this.setState({ansType: parseInt(event.target.value)});
        this.forceUpdate();
    }

    pushQuestion = (props: QuestionAnswer) => {
        this.question.answers.push(props);
        this.forceUpdate();
    }

    pushAccurateQuestion = (props: QuestionAnswer) => {
        this.question.answers = [];
        this.question.answers.push(props);
        this.forceUpdate();
    }

    Rendering(props: {value: boolean, type: number}) {
        const value = props.value;
        const ansType = props.type;
        if (value && ansType != 0) {
            return <CreateAnswer func={this.pushQuestion}/>
        } else {
            return null;
        }
    }

    сreateQuestions = () => {
        if (this.inputText.current != null) {
            const newQuest = new Question;
            newQuest.text = this.inputText.current.value;
            newQuest.ans_type = this.question.ans_type;
            newQuest.answers = this.question.answers;
            newQuest.position = this.props.pos;
            this.setState(({count} : any) => ({
                count: count + 1,
            }));
            this.props.func(newQuest);
            this.question = new Question;
            this.question.answers = [];
        }
    }

    editType = (type: number) => {
        this.question.ans_type = type;
        this.forceUpdate();
    }

    update = () => {
        this.forceUpdate();
    }

    editAnsw = (id_quest: number, id_answ: number, str: string) => {
        this.question.answers[id_answ].text = str;
        this.forceUpdate();
    }

    render() {
        return(
            <div>
                <hr></hr>
                <textarea className='input3' ref={this.inputText} placeholder='Введите вопрос'></textarea>
                <select className='select' onChange={this.handleChange}>
                    <option className='select_head'>Выберите тип вопроса</option>
                    {this.question.ans_type == 0 ? 
                        <option selected={true} className='select_item' value={AnswersType.ACCURATE}>Развернутый ответ</option>
                        :   <option className='select_item' value={AnswersType.ACCURATE}>Развернутый ответ</option>}
                    {this.question.ans_type == 1 ? 
                        <option selected={true} className='select_item' value={AnswersType.SELECT}>Один верный ответ</option>
                        :   <option className='select_item' value={AnswersType.SELECT}>Один верный ответ</option>}
                    {this.question.ans_type == 2 ? 
                        <option selected={true} className='select_item' value={AnswersType.MULTISELECT}>Несколько верных ответов</option>
                        : <option className='select_item' value={AnswersType.MULTISELECT}>Несколько верных ответов</option>}
                </select>
                <ol>
                    {this.question.answers && this.question.answers.map((ans, index) =>
                    <li key={index}>
                        {ans.text}
                        <EditAnsw question={this.question} task={null} answ={ans} edit={this.editAnsw}/>
                        {this.question.ans_type != 0 ?
                        <div>
                            <button 
                                className={!ans.is_correct ? 'button3_2' : 'button3'}
                                onClick={() => {
                                    ans.is_correct = false;
                                    this.forceUpdate(); }
                                }>
                                Ответ неверный
                            </button>
                            <button 
                                className={ans.is_correct ? 'button3_2' : 'button3'}
                                onClick={() => {
                                    ans.is_correct = true;
                                    this.forceUpdate(); }
                                }>
                                Ответ верный
                            </button>
                            </div>
                        : null}
                        <button className='button4' onClick={() => {
                            this.question.answers = this.question.answers.filter(el => el != ans);
                            this.forceUpdate();
                        }}>
                            Удалить
                        </button>
                    </li>)}
                </ol>
                {this.question.ans_type != 0 ?
                    <div>  
                        <button className='button3' onClick={() => this.setState({value: !this.state.value})}>
                        {!this.state.value ? "+Добавить ответ" : "Закрыть"}
                        </button>
                        <this.Rendering value={this.state.value} type={this.question.ans_type}/>
                    </div>
                : <CreateAccurateAnsw func={this.pushAccurateQuestion}/>}
                <br></br>
                <button className='button5' onClick={() => {this.сreateQuestions();}}>Создать вопрос</button>
                <br></br>
                <br></br>
                <br></br>
                <hr></hr>
            </div>
        );
    }
} 

export default CreateQuestion;