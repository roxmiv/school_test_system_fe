import {FunctionComponent, useContext, useEffect, useState} from "react";
import {ApiServices} from "../contexts/ApiStore";
import { MainUser } from "../models/users_auth";
import "../App.css";

interface PersonalAreaProps {
}

export const PersonalArea: FunctionComponent<PersonalAreaProps> = () => {
    const {tasksService} = useContext(ApiServices);
    const [user, setUser] = useState<MainUser|null>(null);

    useEffect(() => {
        tasksService.getUser().then((data: MainUser) => {
            setUser(data);
        },)
    }, [tasksService]);

    const logout = async () => {
        await tasksService.logout({});
        window.location.replace("/home");
    }

    return (
        <div>
            { user ?
            <div className="list13">
                <br></br>
                <br></br>
                <div>Имя: {user.first_name}</div>
                <br></br>
                <div>Фамилия: {user.last_name}</div>
                <br></br>
                <div>Логин: {user.username}</div>
                <br></br>
                <div>Роль: {user.is_school_admin ? "Администратор" : 
                                user.is_teacher ? "Учитель" : 
                                user.is_student ? "Ученик" : "Нет"}</div>
                <br></br>
                <button className="button4" onClick={logout}>Выйти</button>
            </div>
            : null}
        </div>
    );
};