import {FunctionComponent, useContext, useEffect, useState} from "react";
import {ApiServices} from "../contexts/ApiStore";
import { useParams } from 'react-router-dom';
import { Variant } from "../models/Tests";
import "../App.css";
import React from "react";
import { Question, QuestionAnswer } from "../models/Tasks";
import EndText from "./EndText";

interface PageProps {
}

interface ParamTypes {
    param: string;
}

class AccurateAnsw extends React.Component<any, any>{
    inputText: React.RefObject<HTMLInputElement>;
    static contextType = ApiServices
    context!: React.ContextType<typeof ApiServices>

    constructor(props: any) {
        super(props);
        this.inputText = React.createRef();
    }

    func = async (e: any) => {
        let tasksService = this.context;
        let ans = new QuestionAnswer();
        ans.text = e.target.value;
        let data = {
            variant_task: this.props.task,
            question: this.props.question,
            answer: ans,
        }
        await tasksService.tasksService.saveAnswer(data);
    }

    render() {
        return(
            <div>
                <input ref={this.inputText} onChange={this.func}/>
            </div>
        );
    }
}

class SelectAnsw extends React.Component<any, any>{
    question: Question;
    static contextType = ApiServices
    context!: React.ContextType<typeof ApiServices>

    constructor(props: any) {
        super(props);
        this.question = this.props.question;
        this.state = {
            selectedOption: 0,
        }
    }

    func = async (event: any) => {
        let tasksService = this.context;
        this.setState({selectedOption: event?.target.value})
        let ind = this.question.answers.findIndex(el => el.id == event.target.value)
        let data = {
            variant_task: this.props.task,
            question: this.props.question,
            answer: this.question.answers[ind],
        }
        console.log(this.question.answers[ind]);
        await tasksService.tasksService.saveAnswer(data);
    }

    render() {
        return(
            <div className='list11'>
            {this.question.answers && this.question.answers.map((ans, index) => 
                <div key={index}>
                    <input type="radio" value={ans.id} checked={this.state.selectedOption == ans.id} onChange={this.func} />
                        {ans.text}
                </div>
            )}
            </div>
        );
    }
}

class Checkbox extends React.Component<any, any> {
    static contextType = ApiServices
    context!: React.ContextType<typeof ApiServices>
    answer: QuestionAnswer;
    question: Question;

    constructor(props: any) {
        super(props);
        this.answer = this.props.answer;
        this.question = this.props.question;
        this.state = {
            is_checked: false,
        }
    }

    func = async () => {
        let tasksService = this.context;
        this.setState({is_checked: !this.state.is_checked});
        let data = {
                variant_task: this.props.task,
                question: this.props.question,
                answer: this.answer,
        }
        await tasksService.tasksService.saveAnswer(data);
        // console.log(11212);
    }

    render() {
        return(
            <div className='list11'>
                <input type="checkbox" value={this.answer.id} checked={this.state.is_checked} onChange={this.func} />
                {this.answer.text}
            </div>
        );
    }
}

class MultiselectAnsw extends React.Component<any, any>{
    question: Question;
    static contextType = ApiServices
    context!: React.ContextType<typeof ApiServices>

    constructor(props: any) {
        super(props);
        this.question = this.props.question;
    }

    render() {
        return(
            <div>
            {this.question.answers && this.question.answers.map((ans, index) => 
                <div key={index}>
                    <Checkbox question={this.question} answer={ans} task={this.props.task}/>
                </div>
            )}
            </div>
        );
    }
}

class Doing extends React.Component<any, any>{
    static contextType = ApiServices
    context!: React.ContextType<typeof ApiServices>
    variant: Variant;

    constructor(props: any) {
        super(props);
        this.variant = this.props.variant;
        this.state = {
            finished: false,
            score: 0,
            max_score: 0,
        }
    }

    check = async () => {
        let tasksService = this.context;
        let data = {
            variant: this.variant,
        }
        let a = await tasksService.tasksService.checkAnswer(data);
        this.setState({finished: !this.state.finished});
        this.setState({score: a.score});
        this.setState({max_score: a.max_score});
        window.location.replace("/tests/test/end");
    }

    breakTest = () => {
        this.check();
    }

    render () {
        return (
            <>
                {!this.state.finished ?
                <div>
                    <br></br>
                    <header className="header">Тест</header>
                    <br></br>
                    <br></br>
                    <ul className="links">
                        {this.variant?.variant_tasks && this.variant?.variant_tasks.map((task, index) =>
                            <ul key={index}>
                            {task.mask_task.questions && task.mask_task.questions.map((question, index1) =>
                                <ul key={index1}>
                                    <li>
                                        {question.text}
                                        {question.ans_type == 0 ? <AccurateAnsw task={task} question={question}/> : null}
                                        {question.ans_type == 1 ? <SelectAnsw task={task} question={question}/> : null}
                                        {question.ans_type == 2 ? <MultiselectAnsw task={task} question={question}/> : null}
                                        <br></br>
                                    </li>
                                </ul>
                            )}  
                            </ul> 
                        )}
                    </ul>
                    <ul>
                        <button className='button5_2' onClick={this.check}>Завершить</button>
                    </ul>
                </div>
                : 
                <div>
                    <div className="header_home">
                        Тест окончен
                    </div>
                    <button className="button2_2" onClick={() => window.location.replace('/tests')}>Вернуться</button>
                </div>
                }
            </>
        );
    }
};

const padTime = (time: number) => {
    return String(time).length === 1 ? `0${time}` : `${time}`;
};
  
const format = (time: number) => {
    const minutes = Math.floor(time / 60);
    const seconds = time % 60;
    return `${minutes}:${padTime(seconds)}`;
};

export default function EndOfTest(props: {props: Variant})  {
    const {tasksService} = useContext(ApiServices);
    let data = {
        variant: props,
    }
    React.useEffect(() => {
        tasksService.checkAnswer(data);
        return () => {}
    });
  
    return (
        <div>
            <div className="header_home">
                Тест окончен
            </div>
            <button className="button2_2" onClick={() => window.location.replace('/tests')}>Вернуться</button>
        </div>
    );
}

export const DoTest: FunctionComponent<PageProps> = () => {
    const {tasksService} = useContext(ApiServices);
    const {param} = useParams<ParamTypes>();
    const [variant, setVariant] = useState<Variant|null>(null);
    const [counter, setCounter] = React.useState(20);

    useEffect(() => {
        tasksService.getVariant(param).then((data: Variant) => {
            setVariant(data);
            console.log(data);
            setCounter(data.student_test.test.duration_in_minutes * 60);
        },)
    }, [tasksService]);

    useEffect(() => {
      let timer: NodeJS.Timeout;
      if (counter > 0) {
        timer = setTimeout(() => setCounter(c => c - 1), 1000);
      }
  
      return () => {
        if (timer) {
          clearTimeout(timer);
        }
      };
    }, [counter]);

    return (
        <div>
            {counter === 0 ? 
                <div>
                    {variant ?
                        <EndOfTest props={variant}/>
                    : null}
                </div>
            : null}
            {variant && counter != 0 ? 
                <div>
                    <Doing className='list11' variant={variant}/>
                    <br></br>
                    <div className="list14">Осталось времени: {format(counter)}</div>
                </div>
            : null}
        </div>
    );
};