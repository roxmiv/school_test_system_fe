import React, { Component } from 'react';
import {ApiServices} from "../contexts/ApiStore";
import tasksService from "../services/tasksService";

class CreateUser extends React.Component {

    static contextType = ApiServices
    context!: React.ContextType<typeof ApiServices>

    name: React.RefObject<HTMLTextAreaElement>;
    surname: React.RefObject<HTMLTextAreaElement>;
    login: React.RefObject<HTMLTextAreaElement>;
    password: React.RefObject<HTMLTextAreaElement>;

    constructor(props: any){
        super(props);
        this.name = React.createRef();
        this.surname = React.createRef();
        this.login = React.createRef();
        this.password = React.createRef();
    }

    create = async () => {
        if (this.name.current != null &&
            this.surname.current != null &&
            this.login.current != null &&
            this.password.current != null) {
            let Login = this.login.current.value;
            let Password = this.password.current.value;
            let tasksService = this.context;
            let data = {
                username: Login,
                password: Password,
            }
            await tasksService.tasksService.login(data);
        }
    }

    pushUser = async () => {
        if (this.name.current != null &&
            this.surname.current != null &&
            this.login.current != null &&
            this.password.current != null) 
        {
            let Name = this.name.current.value;
            let Surname = this.surname.current.value;
            let Login = this.login.current.value;
            let Password = this.password.current.value;
            let tasksService = this.context;
            let data = {
                username: Login,
                password: Password,
                first_name: Name,
                last_name: Surname,
            }
            await tasksService.tasksService.createUser(data);
            await tasksService.tasksService.login(data);
            window.location.replace("/home");
        }
    }

    render() {
        return(
            <div>
                <div>
                    <textarea className='input' ref={this.name} placeholder='Введите имя'></textarea>
                </div>
                <div>
                    <textarea className='input' ref={this.surname} placeholder='Введите фамилию'></textarea>
                </div>
                <div>
                    <textarea className='input' ref={this.login} placeholder='Введите логин'></textarea>
                </div>
                <div>
                    <textarea className='input' ref={this.password} placeholder='Введите пароль'></textarea>
                </div>
                <div>
                    <button className='button' onClick={this.pushUser}>{"Зарегестрироваться"}</button>
                </div>
            </div>
        );
    }
} 

export default CreateUser;