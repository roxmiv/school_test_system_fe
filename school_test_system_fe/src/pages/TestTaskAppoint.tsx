import {FunctionComponent, useContext, useEffect, useState} from "react";
import {ApiServices} from "../contexts/ApiStore";
import { Grade, Difficult, GradeDifficulty, Topic, Subject } from '../models/Tasks';
import "../App.css";
import "../Select.css";
import tasksService from "../services/tasksService";

function AppointTestTask(props: any) {
    const {tasksService} = useContext(ApiServices);
    const [difficulties, setDifficulties] = useState<GradeDifficulty[]>([]);
    const [difficulty, setDifficulty] = useState<GradeDifficulty|null>(null);
    let grade: Grade|null;
    let subject: Subject|null;

    const taskDifficulty = async (event: any) => {
        console.log(event.target.value);
        let diff = await tasksService.GetDifficultyByGrade(props.grade.id, event.target.value);
        props.func(diff);
    }

    return(
        <div>
            <select className="select" onChange={taskDifficulty}>
                <option className='select_head'>Выберите сложность задания</option>
                <option className='select_item' value={Difficult.EASY}>Просто</option>
                <option className='select_item' value={Difficult.MEDIUM}>Средне</option>
                <option className='select_item' value={Difficult.HARD}>Сложно</option>
            </select>
        </div>
    );
}

export default AppointTestTask;
