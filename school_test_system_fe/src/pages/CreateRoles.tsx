import React, { Component } from 'react';
import {ApiServices} from "../contexts/ApiStore";
import "../App.css";

class CreateRoles extends React.Component<any, any> {

    static contextType = ApiServices
    context!: React.ContextType<typeof ApiServices>
    isTeacher: boolean;
    isStudent: boolean;

    constructor(props: any){
        super(props);
        this.isTeacher = false;
        this.isStudent = false;
        this.handleChange = this.handleChange.bind(this);
        this.onclick = this.onclick.bind(this);
    }    

    handleChange = (event: any) => {
        if (event.target.value == "teacher") {
            this.isTeacher = true;
        } else if (event.target.value == "student"){
            this.isStudent = true;
        }
    }

    onclick = async () => {
        let tasksService = this.context;
        let a = this.props.user;
        console.log(a);
        let data = {
            main_user: a,
        }
        if (this.isTeacher) {
            tasksService.tasksService.createTeacher(data);
        }
        if (this.isStudent) {
            tasksService.tasksService.createStudent(data);
        }
        console.log(a);
    }

    render() {
        return(
            <div>
                <select className='select' onChange={this.handleChange}>
                    <option className='select_head'>Выберите роль пользователя</option>
                    <option className='select_item' value={"teacher"}>Учитель</option>
                    <option className='select_item' value={"student"}>Ученик</option>
                </select>
                <button onClick={this.onclick}>Назначить</button>
            </div>
        );
    }
} 

export default CreateRoles;