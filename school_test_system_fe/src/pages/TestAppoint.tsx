import React, {FunctionComponent, useContext, useEffect, useState} from "react";
import {ApiServices} from "../contexts/ApiStore";
import { Grade, Difficult, GradeDifficulty, Topic, Subject } from '../models/Tasks';
import "../App.css";
import "../Select.css";

function AppointTest(props: any) {
    const {tasksService} = useContext(ApiServices);
    const [grades, setGrades] = useState<Grade[]>([]);
    const [difficulties, setDifficulties] = useState<GradeDifficulty[]>([]);
    const [difficulty, setDifficulty] = useState<GradeDifficulty|null>(null);
    const [subjects, setSubjects] = useState<Subject[]>([]);
    let grade: Grade|null;
    let subject: Subject|null;

    useEffect(() => {
        tasksService.getGradesList().then((data: Grade[]) => {
            setGrades(data);
        },)
    }, [tasksService]);

    const gradeChange = async (event: any) => {
        grade = await tasksService.getGradeName(event?.target.value);
        console.log(grade);
        if (grade) {
            tasksService.getSubjectsListByGrade(grade.id).then((data: Subject[]) => {
                setSubjects(data);
            },)
            tasksService.getDifficultiesList(grade.id).then((data: GradeDifficulty[]) => {
                setDifficulties(data);
            },)
        }
    }

    const SubjectChange = async (event: any) => {
        subject = await tasksService.getSubjectName(event?.target.value);
        props.func(subject);
    }


    const testDifficulty = async (event: any) => {
        await setDifficulty(difficulties[event.target.value]);
    }

    const appoint = async () => {
        props.push(difficulty);
        console.log(difficulty);
    }

    return(
        <div>
            <hr></hr>
                <label>Выберите класс:</label>
            <select className="select" onChange={gradeChange}>
                <option className='select_head'>Выберите класс</option>
                {grades && grades.map((grade, index) =>
                    <option key={index} className='select_item' value={grade.id}>{grade.name}</option>
                )}
            </select>
            <hr></hr>
            <div>Выберите предмет:</div>
            <select className="select" onChange={SubjectChange}>
                <option className='select_head'>Выберите предмет</option>
                {subjects && subjects.map((subject, index) =>
                    <option key={index} className='select_item' value={subject.id}>{subject.name}</option>
                )}
            </select>
            <hr></hr>
            <div>Выберите сложность:</div>
            <select className="select" onChange={testDifficulty}>
                <option className='select_head'>Выберите сложность теста</option>
                <option className='select_item' value={Difficult.EASY}>Просто</option>
                <option className='select_item' value={Difficult.MEDIUM}>Средне</option>
                <option className='select_item' value={Difficult.HARD}>Сложно</option>
            </select>
            <button onClick={appoint}>Подтвердить</button>
        </div>
    );
}

export default AppointTest;
