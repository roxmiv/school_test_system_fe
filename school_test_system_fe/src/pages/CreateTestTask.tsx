import React, { Component } from 'react';
import {
    Question,
    AnswersType,
    QuestionAnswer,
    Topic,
    Subject,
    Grade,
    Difficult,
    GradeDifficulty
} from '../models/Tasks';
import {ApiServices} from "../contexts/ApiStore";
import CreateAnswer from "./CreateAnswer";
import "../App.css";
import "../Select.css";
import {TestTask} from "../models/Tests";
import {TopicTestPage} from "./TopicTest";
import AppointTestTask from "./TestTaskAppoint";

class CreateTestTask extends React.Component<any, any> {
    static contextType = ApiServices;
    inputText: React.RefObject<HTMLInputElement>;
    test_task: TestTask;
    subject: Subject;
    grade: Grade;


    constructor(props: {subject : Subject}){
        super(props);
        this.inputText = React.createRef();
        this.state = {
            value: false,
            count: 1,
        };
        this.test_task = new TestTask();
        this.test_task.topic = new Topic();
        this.subject = this.props.subject;
        this.grade = this.props.subject.grade;
        this.test_task.grade_difficulty = new GradeDifficulty();
    }
    pushTopic = (props: Topic) => {
        this.test_task.topic = props;
    }

    pushGradeDifficulty = (props: GradeDifficulty) => {
        this.test_task.grade_difficulty = props;
        console.log(this.test_task.grade_difficulty);
    }

    checkButton = (props: Topic) => {
        if (this.test_task.topic) {
            return this.test_task.topic.name === props.name;
        }
        return false;
    }

    createTestTasks = () => {
        if (this.test_task.topic != null) {
            const newTestTask = new TestTask();
            newTestTask.topic = this.test_task.topic;
            newTestTask.position = this.state.count;
            newTestTask.grade_difficulty = this.test_task.grade_difficulty;
            this.setState(({count} : any) => ({
                count: count + 1,
            }));
            this.props.func(newTestTask);
            this.test_task = new TestTask();
        }
    }

    funcUpdate = () => {
        this.forceUpdate();
    }

    render() {
        return(
            <div>
                {console.log(this.props.subject.id)}
                <TopicTestPage subject={this.subject} func={this.pushTopic} func_check={this.checkButton} f={this.funcUpdate}/>
                <AppointTestTask func={this.pushGradeDifficulty} grade={this.grade}/>
                <button className='button4' onClick={() => {this.createTestTasks();}}>Создать тестовое задание</button>
            </div>
        );
    }
}

export default CreateTestTask;