import {FunctionComponent, useContext, useEffect, useState} from "react";
import {ApiServices} from "../contexts/ApiStore";
import { Grade, Difficult, GradeDifficulty, Topic, Subject } from '../models/Tasks';
import "../App.css";
import "../Select.css";


function AddTopic(props: any) {
    const {tasksService} = useContext(ApiServices);
    const [topics, setTopics] = useState<Topic[]>([]);
    const [topic, setTopic] = useState<Topic|null>(null);

    useEffect(() => {
        tasksService.getAllTopics().then((data: Topic[]) => {
            setTopics(data);
        },)
    }, [tasksService]);

    const topicChange = async (event: any) => {
        let t = await tasksService.getTopic(event?.target.value);
        setTopic(t);
    }

    const appoint = async () => {
        let s = await tasksService.getSubjectName(props.id);
        topic?.subjects.push(s);
        console.log(topic);
        tasksService.editTopic(topic);
        window.location.reload();
    }

    return(
        <div>
            <select className="select" onChange={topicChange}>
                <option className='select_head'>Выберите тему</option>
                {topics && topics.map((topic, index) =>
                    <option key={index} className='select_item' value={topic.id}>{topic.name} ({topic.subjects[0].name})</option>    
                )}
            </select>
            <button onClick={appoint}>Назначить</button>
        </div>
    );
}

export default AddTopic;
