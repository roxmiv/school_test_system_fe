import {useSnackbar, VariantType, SnackbarProviderProps} from 'notistack';

export function useNotification() {
    const {enqueueSnackbar} = useSnackbar();
    return (message: string, type: VariantType = "default", options?: SnackbarProviderProps) => {
        return enqueueSnackbar(message, {
            anchorOrigin: {
                vertical: 'top',
                horizontal: 'center',
            },
            variant: type,
            autoHideDuration: 2000,
            preventDuplicate: true,
            ...options
        })
    }
}
